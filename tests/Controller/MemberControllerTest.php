<?php

/**
 * This file is part of the Allmega Auth Bundle package.
 *
 * @copyright Allmega 
 * @package   Auth Bundle
 * @author    Eduard Jung <eddie@allmega.de>
 * @license   https://opensource.org/licenses/gpl-license.php GNU Public License
 */

namespace Allmega\AuthBundle\Tests\Controller;

use Allmega\AuthBundle\Controller\MemberController;
use Allmega\BlogBundle\Model\AllmegaWebTest;
use Allmega\AuthBundle\Data;
use Symfony\Component\VarExporter\Exception\ClassNotFoundException;

class MemberControllerTest extends AllmegaWebTest
{
    /**
     * @throws ClassNotFoundException
     */
    public function testIndex(): void
    {
        $this->runTests($this->index, Data::USER_ROLE);
    }

    /**
     * @throws ClassNotFoundException
     */
    public function testRenderPdf(): void
    {
        $this->runTests('print', Data::USER_ROLE);
    }

    /**
     * @throws ClassNotFoundException
     */
    public function testOnline(): void
    {
        $this->runTests('online', Data::USER_ROLE, false, false);
    }

    protected function getRouteName(string $name): string 
    {
        return MemberController::ROUTE_NAME . $name;
    }
}