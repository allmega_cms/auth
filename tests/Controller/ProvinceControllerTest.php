<?php

/**
 * This file is part of the Allmega Auth Bundle package.
 *
 * @copyright Allmega 
 * @package   Auth Bundle
 * @author    Eduard Jung <eddie@allmega.de>
 * @license   https://opensource.org/licenses/gpl-license.php GNU Public License
 */

namespace Allmega\AuthBundle\Tests\Controller;

use Allmega\AuthBundle\Controller\ProvinceController;
use Allmega\BlogBundle\Model\AllmegaWebTest;
use Allmega\AuthBundle\Entity\Province;
use Allmega\AuthBundle\Data;
use Symfony\Component\VarExporter\Exception\ClassNotFoundException;

class ProvinceControllerTest extends AllmegaWebTest
{
    /**
     * @throws ClassNotFoundException
     */
    public function testIndex(): void
    {
        $this->runTests($this->index, Data::ADMIN_ROLE);
    }

    /**
     * @throws ClassNotFoundException
     */
    public function testAdd(): void
    {
        $this->runTests($this->add, Data::ADMIN_ROLE);
    }

    /**
     * @throws ClassNotFoundException
     */
    public function testEdit(): void
    {
        $this->runTests($this->edit, Data::ADMIN_ROLE, true);
    }

    /**
     * @throws ClassNotFoundException
     */
    public function testState(): void
    {
        $this->runModifyTests(role: Data::ADMIN_ROLE, redirectName: $this->index);
    }

    /**
     * @throws ClassNotFoundException
     */
    protected function create(): void
    {
        $country = $this->findUserByRole(Data::ADMIN_ROLE)->getCountry();
        $entity = Province::build(country: $country);
        $this->em->persist($entity);
        $this->em->flush();
        $this->params->setEntity($entity);
    }

    protected function getRouteName(string $name): string 
    {
        return ProvinceController::ROUTE_NAME . $name;
    }
}