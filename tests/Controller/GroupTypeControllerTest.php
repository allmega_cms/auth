<?php

/**
 * This file is part of the Allmega Auth Bundle package.
 *
 * @copyright Allmega 
 * @package   Auth Bundle
 * @author    Eduard Jung <eddie@allmega.de>
 * @license   https://opensource.org/licenses/gpl-license.php GNU Public License
 */

namespace Allmega\AuthBundle\Tests\Controller;

use Allmega\AuthBundle\Controller\GroupTypeController;
use Allmega\BlogBundle\Model\AllmegaWebTest;
use Allmega\AuthBundle\Entity\GroupType;
use Allmega\AuthBundle\Data;
use Symfony\Component\VarExporter\Exception\ClassNotFoundException;

class GroupTypeControllerTest extends AllmegaWebTest
{
    /**
     * @throws ClassNotFoundException
     */
    public function testIndex(): void
    {
        $this->runTests($this->index, Data::ADMIN_ROLE);
    }

    /**
     * @throws ClassNotFoundException
     */
    public function testAdd(): void
    {
        $this->runTests($this->add, Data::ADMIN_ROLE);
    }

    /**
     * @throws ClassNotFoundException
     */
    public function testEdit(): void
    {
        $this->runTests($this->edit, Data::ADMIN_ROLE, true);
    }

    /**
     * @throws ClassNotFoundException
     */
    public function testDelete(): void
    {
        $this->runModifyTests(role: Data::ADMIN_ROLE, delete: true);
    }

    protected function create(): void
    {
        $entity = GroupType::build();
        $this->em->persist($entity);
        $this->em->flush();
        $this->params->setEntity($entity);
    }

    protected function getRouteName(string $name): string 
    {
        return GroupTypeController::ROUTE_NAME . $name;
    }
}