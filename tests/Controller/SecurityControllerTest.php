<?php

/**
 * This file is part of the Allmega Auth Bundle package.
 *
 * @copyright Allmega 
 * @package   Auth Bundle
 * @author    Eduard Jung <eddie@allmega.de>
 * @license   https://opensource.org/licenses/gpl-license.php GNU Public License
 */

namespace Allmega\AuthBundle\Tests\Controller;

use Allmega\AuthBundle\Controller\SecurityController;
use Allmega\BlogBundle\Model\AllmegaWebTest;
use Symfony\Component\VarExporter\Exception\ClassNotFoundException;

class SecurityControllerTest extends AllmegaWebTest
{
    /**
     * @throws ClassNotFoundException
     */
    public function testLogin(): void
    {
        $this->runPublicTest('login');
    }

    /**
     * @throws ClassNotFoundException
     */
    public function testMenuIcon(): void
    {
        $this->runPublicTest($this->icon);
    }

    protected function getRouteName(string $name): string 
    {
        return SecurityController::ROUTE_NAME . $name;
    }
}