<?php

/**
 * This file is part of the Allmega Auth Bundle package.
 *
 * @copyright Allmega 
 * @package   Auth Bundle
 * @author    Eduard Jung <eddie@allmega.de>
 * @license   https://opensource.org/licenses/gpl-license.php GNU Public License
 */

namespace Allmega\AuthBundle\Tests\Controller;

use Allmega\AuthBundle\Controller\UserController;
use Allmega\BlogBundle\Model\AllmegaWebTest;
use Allmega\AuthBundle\Entity\User;
use Allmega\AuthBundle\Data;
use Symfony\Component\VarExporter\Exception\ClassNotFoundException;

class UserControllerTest extends AllmegaWebTest
{
    /**
     * @throws ClassNotFoundException
     */
    public function testDashboardWidget(): void
    {
        $this->runTests($this->dashboard, Data::ADMIN_ROLE, false, false);
    }

    /**
     * @throws ClassNotFoundException
     */
    public function testSearch(): void
    {
        $this->runTests($this->search, Data::ADMIN_ROLE);
    }

    /**
     * @throws ClassNotFoundException
     */
    public function testIndex(): void
    {
        $this->runTests($this->index, Data::ADMIN_ROLE);
    }

    /**
     * @throws ClassNotFoundException
     */
    public function testShow(): void
    {
        $this->runTests($this->show, Data::ADMIN_ROLE, true);
    }

    /**
     * @throws ClassNotFoundException
     */
    public function testAdd(): void
    {
        $this->runTests($this->show, Data::ADMIN_ROLE, true);
    }

    /**
     * @throws ClassNotFoundException
     */
    public function testEdit(): void
    {
        $this->runTests($this->edit, Data::ADMIN_ROLE, true);
    }

    /**
     * @throws ClassNotFoundException
     */
    public function testChangePassword(): void
    {
        $this->runTests('change_password', Data::ADMIN_ROLE, true);
    }

    /**
     * @throws ClassNotFoundException
     */
    public function testChangeState(): void
    {
        $this->runModifyTests(
            role: Data::ADMIN_ROLE,
            fnRedirectParams: 'getIdAsRouteParams');
    }

    /**
     * @throws ClassNotFoundException
     */
    public function testDelete(): void
    {
        $this->runModifyTests(role: Data::ADMIN_ROLE, delete: true);
    }

    /**
     * @throws ClassNotFoundException
     */
    protected function create(): void
    {
        $groups = [$this->findGroupByShortname(Data::USER_GROUP)];
        $user = $this->findUserByRole(Data::ADMIN_ROLE);

        $entity = User::build(
            groups: $groups,
            country: $user->getCountry(),
            province: $user->getProvince()
        );
        
        $this->em->persist($entity);
        $this->em->flush();
        $this->params->setEntity($entity);
    }

    protected function getRouteName(string $name): string 
    {
        return UserController::ROUTE_NAME . $name;
    }
}