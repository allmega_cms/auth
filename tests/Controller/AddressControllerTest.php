<?php

/**
 * This file is part of the Allmega Auth Bundle package.
 *
 * @copyright Allmega 
 * @package   Auth Bundle
 * @author    Eduard Jung <eddie@allmega.de>
 * @license   https://opensource.org/licenses/gpl-license.php GNU Public License
 */

namespace Allmega\AuthBundle\Tests\Controller;

use Allmega\AuthBundle\Data;
use Allmega\AuthBundle\Entity\Address;
use Allmega\AuthBundle\Controller\{AddressController, ProfileController};
use Allmega\BlogBundle\Model\AllmegaWebTest;
use Random\RandomException;
use Symfony\Component\VarExporter\Exception\ClassNotFoundException;

class AddressControllerTest extends AllmegaWebTest
{
    /**
     * @throws ClassNotFoundException
     */
    public function testAdd(): void
    {
        $this->runTests($this->add, Data::USER_ROLE);
    }

    /**
     * @throws ClassNotFoundException
     */
    public function testEdit(): void
    {
        $this->runTests($this->edit, Data::USER_ROLE, true);
    }

    /**
     * @throws ClassNotFoundException
     */
    public function testDelete(): void
    {
        $redirectRoute = ProfileController::ROUTE_NAME . $this->show;
        $this->runModifyTests(
            role: Data::USER_ROLE,
            delete: true,
            redirectRoute: $redirectRoute);
    }

    /**
     * @throws ClassNotFoundException
     * @throws RandomException
     */
    protected function create(): void
    {
        $user = $this->findUserByRole(Data::USER_ROLE);
        $entity = Address::build(user: $user);
        $this->em->persist($entity);
        $this->em->flush();
        $this->params->setEntity($entity);
    }

    protected function getRouteName(string $name): string 
    {
        return AddressController::ROUTE_NAME . $name;
    }
}