<?php

/**
 * This file is part of the Allmega Auth Bundle package.
 *
 * @copyright Allmega 
 * @package   Auth Bundle
 * @author    Eduard Jung <eddie@allmega.de>
 * @license   https://opensource.org/licenses/gpl-license.php GNU Public License
 */

namespace Allmega\AuthBundle\Tests\Controller;

use Allmega\AuthBundle\Controller\ProfileController;
use Allmega\BlogBundle\Model\AllmegaWebTest;
use Allmega\AuthBundle\Entity\User;
use Allmega\AuthBundle\Data;
use Symfony\Component\VarExporter\Exception\ClassNotFoundException;

class ProfileControllerTest extends AllmegaWebTest
{
    /**
     * @throws ClassNotFoundException
     */
    public function testDashboardWidget(): void
    {
        $this->runTests($this->dashboard, Data::USER_ROLE, false, false);
    }

    /**
     * @throws ClassNotFoundException
     */
    public function testMenuIcon(): void
    {
        $this->runTests($this->icon, Data::USER_ROLE, false, false);
    }

    /**
     * @throws ClassNotFoundException
     */
    public function testShow(): void
    {
        $this->runTests($this->show, Data::USER_ROLE, true);
    }

    /**
     * @throws ClassNotFoundException
     */
    public function testEdit(): void
    {
        $this->runTests($this->edit, Data::USER_ROLE, true);
    }

    /**
     * @throws ClassNotFoundException
     */
    public function testChangePassword(): void
    {
        $this->runTests('change_password', Data::USER_ROLE, true);
    }

    /**
     * @throws ClassNotFoundException
     */
    protected function create(): void
    {
        $groups = [$this->findGroupByShortname(Data::USER_GROUP)];
        $user = $this->findUserByRole(Data::USER_ROLE);

        $entity = User::build(
            groups: $groups,
            country: $user->getCountry(),
            province: $user->getProvince()
        );
        
        $this->em->persist($entity);
        $this->em->flush();
        $this->params->setEntity($entity);
    }

    protected function getRouteName(string $name): string 
    {
        return ProfileController::ROUTE_NAME . $name;
    }
}