<?php

/**
 * This file is part of the Allmega Auth Bundle package.
 *
 * @copyright Allmega 
 * @package   Auth Bundle
 * @author    Eduard Jung <eddie@allmega.de>
 * @license   https://opensource.org/licenses/gpl-license.php GNU Public License
 */

namespace Allmega\AuthBundle\Entity;

use Allmega\BlogBundle\Utils\Helper;
use Allmega\AuthBundle\Model\BaseProps;
use Allmega\AuthBundle\Repository\CountryRepository;
use Doctrine\Common\Collections\{ArrayCollection, Collection};
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: CountryRepository::class)]
#[ORM\Table(name: '`allmega_auth__country`')]
class Country extends BaseProps
{
    #[ORM\OneToMany(targetEntity: Province::class, mappedBy: 'country')]
    #[ORM\OrderBy(['name' => 'ASC'])]
    private Collection $provinces;

    #[ORM\OneToMany(targetEntity: User::class, mappedBy: 'country')]
    #[ORM\OrderBy(['lastname' => 'ASC'])]
    private Collection $users;
    
    /**
     * Create a new Country entity with predetermined data,  
     * if no data is provided, it will be generated:
     *  - $shortname, $name as dummy text
     */
    public static function build(
        string $shortname = null,
        string $description = null,
        string $name = null): static
    {
        $shortname = $shortname ?? Helper::generateRandomString(3);
        $name = $name ?? Helper::generateRandomString();

        return (new static())
            ->setDescription($description)
            ->setShortname($shortname)
            ->setName($name);
    }

    public function __construct()
    {
        $this->provinces = new ArrayCollection();
        $this->users = new ArrayCollection();
    }

    /**
     * @return Collection<int,Province>
     */
    public function getProvinces(): Collection
    {
        return $this->provinces;
    }

    public function addProvince(Province $province): static
    {
        if (!$this->provinces->contains($province)) {
            $this->provinces[] = $province;
            $province->setCountry($this);
        }
        return $this;
    }

    public function removeProvince(Province $province): static
    {
        if ($this->provinces->contains($province)) {
            $this->provinces->removeElement($province);
            if ($province->getCountry() === $this) $province->setCountry(null);
        }
        return $this;
    }

    /**
     * @return Collection<int,User>
     */
    public function getUsers(): Collection
    {
        return $this->users;
    }

    public function addUser(User $user): static
    {
        if (!$this->users->contains($user)) {
            $this->users[] = $user;
            $user->setCountry($this);
        }
        return $this;
    }

    public function removeUser(User $user): static
    {
        if ($this->users->contains($user)) {
            $this->users->removeElement($user);
            if ($user->getCountry() === $this) $user->setCountry(null);
        }
        return $this;
    }
}