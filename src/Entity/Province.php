<?php

/**
 * This file is part of the Allmega Auth Bundle package.
 *
 * @copyright Allmega 
 * @package   Auth Bundle
 * @author    Eduard Jung <eddie@allmega.de>
 * @license   https://opensource.org/licenses/gpl-license.php GNU Public License
 */

namespace Allmega\AuthBundle\Entity;

use Allmega\BlogBundle\Utils\Helper;
use Allmega\AuthBundle\Model\BaseProps;
use Allmega\AuthBundle\Repository\ProvinceRepository;
use Doctrine\Common\Collections\{ArrayCollection, Collection};
use Doctrine\ORM\Mapping as ORM;
use Doctrine\DBAL\Types\Types;
use DateTimeInterface;

#[ORM\Entity(repositoryClass: ProvinceRepository::class)]
#[ORM\Table(name: '`allmega_auth__province`')]
class Province extends BaseProps
{
    #[ORM\OneToMany(targetEntity: User::class, mappedBy: 'province')]
    #[ORM\OrderBy(['lastname' => 'ASC'])]
    private Collection $users;

    #[ORM\ManyToMany(targetEntity: Holiday::class, inversedBy: 'provinces')]
    #[ORM\JoinTable(name: '`allmega_auth__province_holiday`')]
    #[ORM\OrderBy(['name' => 'ASC'])]
    private Collection $holidays;

    #[ORM\Column(type: Types::BOOLEAN)]
    private bool $active = false;

    #[ORM\ManyToOne(inversedBy: 'provinces')]
    private ?Country $country = null;
        
    /**
     * Create a new Province entity with predetermined data, 
     * if no data is provided, it will be generated:
     * - $name, $shortname as dummy text
     * - $country with shortname 'de' will be created
     */
    public static function build(
        string $description = null,
        Country $country = null,
        string $name = null,
        string $shortname = null): static
    {
        $shortname = $shortname ?? Helper::generateRandomString();
        $country = $country ?? Country::build(shortname: 'de');
        $name = $name ?? Helper::generateRandomString();

        return (new static())
            ->setDescription($description)
            ->setShortname($shortname)
            ->setCountry($country)
            ->setName($name);
    }

    public function __construct()
    {
        $this->users = new ArrayCollection();
        $this->holidays = new ArrayCollection();
    }

    /**
     * @return Collection<int,User>
     */
    public function getUsers(): Collection
    {
        return $this->users;
    }

    public function addUser(User $user): static
    {
        if (!$this->users->contains($user)) {
            $this->users[] = $user;
            $user->setProvince($this);
        }
        return $this;
    }

    public function removeUser(User $user): static
    {
        if ($this->users->contains($user)) {
            $this->users->removeElement($user);
            if ($user->getProvince() === $this) $user->setProvince(null);
        }
        return $this;
    }

    /**
     * @return Collection<int,Holiday>
     */
    public function getHolidays(): Collection
    {
        return $this->holidays;
    }

    public function addHoliday(Holiday $holiday): static
    {
        if (!$this->holidays->contains($holiday)) $this->holidays[] = $holiday;
        return $this;
    }

    public function removeHoliday(Holiday $holiday): static
    {
        if ($this->holidays->contains($holiday)) $this->holidays->removeElement($holiday);
        return $this;
    }

    public function isActive(): bool
    {
        return $this->active;
    }

    public function setActive(bool $active): static
    {
        $this->active = $active;
        return $this;
    }

    public function getCountry(): ?Country
    {
        return $this->country;
    }

    public function setCountry(?Country $country): static
    {
        $this->country = $country;
        return $this;
    }

    public function isDateHoliday(int $year, DateTimeInterface $date)
    {
        foreach ($this->getHolidays() as $holiday) {
            $day = $holiday->setDay($year)->getDay()->format('Y-m-d');
            if ($date->format('Y-m-d') == $day) return $holiday;
        }
        return null;
    }

	public function isDateWorkingDay(DateTimeInterface $date): bool
	{
		$weekDayNum = $date->format('N');
		$curDayHoliday = $this->isDateHoliday($date->format('Y'), $date);
		return !$curDayHoliday && $weekDayNum != 6 && $weekDayNum != 7;
	}
}