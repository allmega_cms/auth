<?php

/**
 * This file is part of the Allmega Auth Bundle package.
 *
 * @copyright Allmega 
 * @package   Auth Bundle
 * @author    Eduard Jung <eddie@allmega.de>
 * @license   https://opensource.org/licenses/gpl-license.php GNU Public License
 */

namespace Allmega\AuthBundle\Entity;

use Allmega\BlogBundle\Utils\Helper;
use Allmega\AuthBundle\Model\BaseProps;
use Allmega\AuthBundle\Repository\HolidayRepository;
use Doctrine\Common\Collections\{ArrayCollection, Collection};
use Doctrine\ORM\Mapping as ORM;
use DateTimeInterface;
use DateInterval;
use DateTime;

#[ORM\Entity(repositoryClass: HolidayRepository::class)]
#[ORM\Table(name: '`allmega_auth__holiday`')]
class Holiday extends BaseProps implements \JsonSerializable
{
    #[ORM\ManyToMany(targetEntity: Province::class, mappedBy: 'holidays')]
    #[ORM\OrderBy(['name' => 'ASC'])]
    private Collection $provinces;

    /** The date of the holiday is calculated automatically */
    private DateTimeInterface $day;
    
    /**
     * Create a new Holiday entity with predetermined data, 
     * if no data is provided, it will be generated:
     * - $shortname, $name as dummy text
     * - $provinces, array with Province entities
     */
    public static function build(
        string $shortname = null,
        string $name = null,
        array $provinces = [],
        string $description = null): static
    {
        $shortname = $shortname ?? Helper::generateRandomString();
        $name = $name ?? Helper::generateRandomString();

        $holiday = (new static())
            ->setDescription($description)
            ->setShortname($shortname)
            ->setName($name);
            
        foreach ($provinces as $province) $holiday->addProvince($province);
        return $holiday;
    }

    public function __construct()
    {
        $this->provinces = new ArrayCollection();
    }

    public function getDay(): DateTimeInterface
    {
        return $this->day;
    }

    public function setDay(int $year): static
    {
        $day = new DateTime();
        $day->setTime(0,0,0);
        $date = date('Y-m-d', easter_date($year));
        $easter = new DateTime($date);
        switch ($this->shortname) {
            case 'new_year':
                $day->setDate($year, 1, 1);
                break;
            case 'holy_three_kings':
                $day->setDate($year, 1, 6);
                break;
            case 'roses_monday':
                $easter->sub(new DateInterval('P48D'));
                $day = $easter;
                break;
            case 'ash_wednesday':
                $easter->sub(new DateInterval('P46D'));
                $day = $easter;
                break;
            case 'good_friday':
                $easter->sub(new DateInterval('P2D'));
                $day = $easter;
                break;
            case 'easter_sunday':
                $day = $easter;
                break;
            case 'easter_monday':
                $easter->add(new DateInterval('P1D'));
                $day = $easter;
                break;
            case 'working_day':
                $day->setDate($year, 5, 1);
                break;
            case 'ascension_christ':
                $easter->add(new DateInterval('P39D'));
                $day = $easter;
                break;
            case 'whit_sunday':
                $easter->add(new DateInterval('P49D'));
                $day = $easter;
                break;
            case 'whit_monday':
                $easter->add(new DateInterval('P50D'));
                $day = $easter;
                break;
            case 'corpus_christi':
                $easter->add(new DateInterval('P60D'));
                $day = $easter;
                break;
            case 'assumption_day':
                $day->setDate($year, 8, 15);
                break;
            case 'german_unity_day':
                $day->setDate($year, 10, 3);
                break;
            case 'reformation_day':
                $day->setDate($year, 10, 31);
                break;
            case 'all_saints_day':
                $day->setDate($year, 11, 1);
                break;
            case 'repentance_day':
                $day->setDate($year, 12, 25);
                $day->sub(new DateInterval('P34D'));
                break;
            case 'christmas_eve':
                $day->setDate($year, 12, 24);
                break;
            case 'first_christmasday':
                $day->setDate($year, 12, 25);
                break;
            case 'second_christmasday':
                $day->setDate($year, 12, 26);
                break;
            case 'sylvester':
                $day->setDate($year, 12, 31);
                break;
        }
        $this->day = $day;
        return $this;
    }

    /**
     * @return Collection<int,Province>
     */
    public function getProvinces(): Collection
    {
        return $this->provinces;
    }

    public function addProvince(Province $province): static
    {
        if (!$this->provinces->contains($province)) {
            $this->provinces[] = $province;
            $province->addHoliday($this);
        }
        return $this;
    }

    public function removeProvince(Province $province): static
    {
        if ($this->provinces->contains($province)) {
            $this->provinces->removeElement($province);
            $province->removeHoliday($this);
        }
        return $this;
    }

    public function jsonSerialize(): array
    {
        return [
            'name' => $this->name,
            'day' => $this->day,
        ];
    }
}