<?php

/**
 * This file is part of the Allmega Auth Bundle package.
 *
 * @copyright Allmega 
 * @package   Auth Bundle
 * @author    Eduard Jung <eddie@allmega.de>
 * @license   https://opensource.org/licenses/gpl-license.php GNU Public License
 */

namespace Allmega\AuthBundle\Entity;

use Allmega\BlogBundle\Utils\Helper;
use Allmega\AuthBundle\Model\BaseProps;
use Allmega\AuthBundle\Repository\GroupTypeRepository;
use Doctrine\Common\Collections\{ArrayCollection, Collection};
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: GroupTypeRepository::class)]
#[ORM\Table(name: '`allmega_auth__group_type`')]
class GroupType extends BaseProps
{
    #[ORM\ManyToMany(targetEntity: Group::class, mappedBy: 'grouptypes')]
    #[ORM\OrderBy(['name' => 'ASC'])]
    private Collection $groups;
    
    /**
     * Create a new GroupType entity with predetermined data, 
     * if no data is provided, it will be generated:
     * - $shortname, $name as dummy text
     */
    public static function build(
        string $shortname = null,
        string $description = null,
        string $name = null): static
    {
        $shortname = $shortname ?? Helper::generateRandomString();
        $name = $name ?? Helper::generateRandomString();

        return (new static())
            ->setDescription($description)
            ->setShortname($shortname)
            ->setName($name);
    }

    public function __construct()
    {
        $this->groups = new ArrayCollection();
    }

    /**
     * @return Collection<int,Group>
     */
    public function getGroups(): Collection
    {
        return $this->groups;
    }

    public function addGroup(Group $group): static
    {
        if (!$this->groups->contains($group)) {
            $this->groups[] = $group;
            $group->addGrouptype($this);
        }

        return $this;
    }

    public function removeGroup(Group $group): static
    {
        if ($this->groups->contains($group)) {
            $this->groups->removeElement($group);
            $group->removeGrouptype($this);
        }

        return $this;
    }
}