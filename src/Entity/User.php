<?php

/**
 * This file is part of the Allmega Auth Bundle package.
 *
 * @copyright Allmega 
 * @package   Auth Bundle
 * @author    Eduard Jung <eddie@allmega.de>
 * @license   https://opensource.org/licenses/gpl-license.php GNU Public License
 */

namespace Allmega\AuthBundle\Entity;

use Allmega\AuthBundle\Data;
use Allmega\AuthBundle\Model\GroupableInterface;
use Allmega\AuthBundle\Repository\UserRepository;
use Allmega\BlogBundle\Utils\{Helper, IdGenerator};
use Allmega\BlogBundle\Model\SortableItemInterface;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Security\Core\User\PasswordAuthenticatedUserInterface;
use Doctrine\Common\Collections\{ArrayCollection, Collection};
use Doctrine\ORM\Mapping as ORM;
use Doctrine\DBAL\Types\Types;
use DateTimeInterface;
use DateTime;

#[UniqueEntity(['username'], message: 'errors.exists')]
#[UniqueEntity(['email'], message: 'errors.exists')]
#[ORM\Entity(repositoryClass: UserRepository::class)]
#[ORM\Table(name: '`allmega_auth__user`')]
class User implements UserInterface, PasswordAuthenticatedUserInterface, GroupableInterface, SortableItemInterface
{
    #[ORM\Id]
    #[ORM\Column(length: 191)]
    #[ORM\GeneratedValue(strategy: 'CUSTOM')]
    #[ORM\CustomIdGenerator(class: IdGenerator::class)]
    private ?string $id = null;

    #[ORM\Column(length: 191, unique: true)]
    #[Assert\NotBlank(message: 'errors.blank')]
    #[Assert\Length(max: 191, maxMessage: 'errors.max_value')]
    private ?string $username = null;

    #[ORM\Column(length: 191, unique: true)]
    #[Assert\NotBlank(message: 'errors.blank')]
    #[Assert\Length(max: 191, maxMessage: 'errors.max_value')]
    private ?string $email = null;

    #[ORM\Column(length: 191)]
    private ?string $password = null;

    private array $roles = [];

    #[ORM\Column(length: 191)]
    #[Assert\NotBlank(message: 'errors.blank')]
    #[Assert\Length(max: 191, maxMessage: 'errors.max_value')]
    private ?string $firstname = null;

    #[ORM\Column(length: 191)]
    #[Assert\NotBlank(message: 'errors.blank')]
    #[Assert\Length(max: 191, maxMessage: 'errors.max_value')]
    private ?string $lastname = null;

    #[ORM\Column(type: Types::DATE_MUTABLE, nullable: true)]
    private ?DateTimeInterface $birthday = null;

    #[ORM\Column(length: 191, nullable: true)]
    #[Assert\Length(max: 191, maxMessage: 'errors.max_value')]
    private ?string $phone = null;

    #[ORM\Column(length: 191, nullable: true)]
    #[Assert\Length(max: 191, maxMessage: 'errors.max_value')]
    private ?string $mobile = null;

    #[ORM\Column(length: 191, nullable: true)]
    #[Assert\Length(max: 191, maxMessage: 'errors.max_value')]
    private ?string $internally = null;

    #[ORM\Column(type: Types::DATETIME_MUTABLE)]
    private DateTimeInterface $created;

    #[ORM\Column(type: Types::DATETIME_MUTABLE)]
    private DateTimeInterface $updated;

    #[ORM\Column(type: Types::DATETIME_MUTABLE, nullable: true)]
    private ?DateTimeInterface $expires = null;

    #[ORM\Column(type: Types::DATETIME_MUTABLE, nullable: true)]
    private ?DateTimeInterface $lastActivity = null;

    #[ORM\Column(type: Types::DATETIME_MUTABLE, nullable: true)]
    private ?DateTimeInterface $lastSeen;

    #[ORM\Column(type: Types::DATETIME_MUTABLE)]
    private DateTimeInterface $lastLogin;

    #[ORM\Column(type: Types::BOOLEAN)]
    private bool $enabled = false;

    #[ORM\Column]
    private bool $verified = false;

    #[ORM\Column(type: Types::BOOLEAN)]
    private bool $deleted = false;

    #[ORM\Column(type: Types::BOOLEAN)]
    private bool $selectable = false;

    #[ORM\OneToMany(targetEntity: Address::class, mappedBy: 'user', orphanRemoval: true)]
    #[ORM\OrderBy(['city' => 'ASC'])]
    private Collection $addresses;

    /**
     * @var Collection<int,Role>
     */
    #[ORM\ManyToMany(targetEntity: Role::class)]
    #[ORM\JoinTable(name: '`allmega_auth__user_permission`')]
    #[ORM\OrderBy(['name' => 'ASC'])]
    private Collection $permissions;

    #[ORM\ManyToMany(targetEntity: Group::class, mappedBy: 'users')]
    #[ORM\OrderBy(['name' => 'ASC'])]
    private Collection $groups;

    #[ORM\ManyToOne(inversedBy: 'users')]
    private ?Province $province = null;

    #[ORM\ManyToOne(inversedBy: 'users')]
    private ?Country $country = null;

    /**
     * Create a new User entity with predetermined data, 
     * if no data is provided, it will be generated:
     * 
     * - $username, $password, $firstname, $lastname, $email as dummy text
     * - $groups, group with shortname 'auth.user' will be created and added to $groups
     * - $country with shortname 'de' will be created
     * - $province with name 'Bayern' will be created
     * 
     * ** Password must be hashed bevor or after creation
     */
    public static function build(
        string $username = null,
        string $password = null,
        string $firstname = null,
        string $lastname = null,
        string $email = null,
        array $groups = [],
        bool $enabled = false,
        bool $verified = false,
        bool $deleted = false,
        array $permissions = [],
        Country $country = null,
        Province $province = null
    ): static {
        $country = $country ?? Country::build(shortname: 'de');
        $province = $province ?? Province::build(country: $country, name: 'Bayern');
        if (!$groups) $groups = [Group::build(shortname: Data::USER_GROUP)];

        $email = $email ?? Helper::generateRandomString(5) . '@home.lan';
        $firstname = $firstname ?? Helper::generateRandomString();
        $username = $username ?? Helper::generateRandomString();
        $password = $password ?? Helper::generateRandomString();
        $lastname = $lastname ?? Helper::generateRandomString();

        $user = (new static())
            ->setFirstname($firstname)
            ->setLastname($lastname)
            ->setUsername($username)
            ->setPassword($password)
            ->setProvince($province)
            ->setCountry($country)
            ->setEnabled($enabled)
            ->setVerified($verified)
            ->setDeleted($deleted)
            ->setEmail($email);

        foreach ($groups as $group) $user->addGroup($group);
        foreach ($permissions as $role) $user->addPermission($role);
        
        return $user;
    }

    public function __construct()
    {
        $now = new DateTime();
        $this->created = $now;
        $this->updated = $now;
        $this->lastSeen = $now;
        $this->lastLogin = $now;
        $this->permissions = new ArrayCollection();
        $this->addresses = new ArrayCollection();
        $this->groups = new ArrayCollection();
    }

    public function getId(): ?string
    {
        return $this->id;
    }

    public function setRoles(array $roles): static
    {
        foreach ($roles as $role) $this->addRole($role);
        return $this;
    }

    public function getRoles(): array
    {
        foreach ($this->permissions as $role) $this->roles[] = $role->getName();

        foreach ($this->getGroups() as $group) {
            foreach ($group->getRoles() as $role) $this->roles[] = $role->getName();
        }
        return array_unique($this->roles);
    }

    public function hasRole(string $role): bool
    {
        return in_array(strtoupper($role), $this->getRoles(), true);
    }

    public function addRole(string $role): static
    {
        if (!$this->hasRole($role)) $this->roles[] = $role;
        return $this;
    }

    public function getPassword(): ?string
    {
        return $this->password;
    }

    public function setPassword(string $password): static
    {
        $this->password = $password;
        return $this;
    }

    public function getSalt(): void
    {
    }

    public function eraseCredentials(): void
    {
    }

    public function getUserIdentifier(): string
    {
        return $this->username;
    }

    public function getUsername(): ?string
    {
        return $this->username;
    }

    public function setUsername(string $username): static
    {
        $this->username = $username;
        return $this;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(string $email): static
    {
        $this->email = $email;
        return $this;
    }

    public function getFirstname(): ?string
    {
        return $this->firstname;
    }

    public function setFirstname(string $firstname): static
    {
        $this->firstname = $firstname;
        return $this;
    }

    public function getLastname(): ?string
    {
        return $this->lastname;
    }

    public function setLastname(string $lastname): static
    {
        $this->lastname = $lastname;
        return $this;
    }

    public function getFullname(): string
    {
        return $this->lastname . ', ' . $this->firstname;
    }

    public function getBirthday(): ?DateTimeInterface
    {
        return $this->birthday;
    }

    public function setBirthday(?DateTimeInterface $birthday): static
    {
        $this->birthday = $birthday;
        return $this;
    }

    public function getPhone(): ?string
    {
        return $this->phone;
    }

    public function setPhone(?string $phone): static
    {
        $this->phone = $phone;
        return $this;
    }

    public function getMobile(): ?string
    {
        return $this->mobile;
    }

    public function setMobile(?string $mobile): static
    {
        $this->mobile = $mobile;
        return $this;
    }
    public function getInternally(): ?string
    {
        return $this->internally;
    }

    public function setInternally(?string $internally): static
    {
        $this->internally = $internally;
        return $this;
    }

    public function getCreated(): DateTimeInterface
    {
        return $this->created;
    }

    public function setCreated(DateTimeInterface $created): static
    {
        $this->created = $created;
        return $this;
    }

    public function getUpdated(): DateTimeInterface
    {
        return $this->updated;
    }

    public function setUpdated(): static
    {
        $this->updated = new DateTime();
        return $this;
    }

    public function getExpires(): ?DateTimeInterface
    {
        return $this->expires;
    }

    public function setExpires(?DateTimeInterface $expires): static
    {
        $this->expires = $expires;
        return $this;
    }

    public function getLastActivity(): DateTimeInterface
    {
        if (!$this->lastActivity) $this->setLastActivity();
        return $this->lastActivity;
    }

    public function setLastActivity(): static
    {
        $this->lastActivity = new DateTime;
        return $this;
    }

    public function getLastSeen(): ?DateTimeInterface
    {
        return $this->lastSeen;
    }

    public function setLastSeen(?DateTimeInterface $lastSeen): static
    {
        $this->lastSeen = $lastSeen;
        return $this;
    }

    public function getLastLogin(): DateTimeInterface
    {
        return $this->lastLogin;
    }

    public function setLastLogin(): static
    {
        $this->lastLogin = new DateTime();
        return $this;
    }

    public function isEnabled(): bool
    {
        return $this->enabled;
    }

    public function setEnabled(bool $enabled): static
    {
        $this->enabled = $enabled;
        return $this;
    }

    public function isVerified(): bool
    {
        return $this->verified;
    }

    public function setVerified(bool $verified): static
    {
        $this->verified = $verified;
        return $this;
    }

    public function isDeleted(): bool
    {
        return $this->deleted;
    }

    public function setDeleted(bool $deleted): static
    {
        $this->deleted = $deleted;
        return $this;
    }

    public function isSelectable(): bool
    {
        return $this->selectable;
    }

    public function setSelectable(bool $selectable): static
    {
        $this->selectable = $selectable;
        return $this;
    }

    /**
     * @return Collection<int,Role>
     */
    public function getPermissions(): Collection
    {
        return $this->permissions;
    }

    public function addPermission(Role $permission): static
    {
        if (!$this->permissions->contains($permission)) {
            $this->permissions->add($permission);
        }
        return $this;
    }

    public function removePermission(Role $permission): static
    {
        $this->permissions->removeElement($permission);
        return $this;
    }

    /**
     * @return Collection<int,Group>
     */
    public function getGroups(): Collection
    {
        return $this->groups;
    }

    public function addGroup(Group $group): static
    {
        if (!$this->groups->contains($group)) {
            $this->groups[] = $group;
            $group->addUser($this);
        }
        return $this;
    }

    public function removeGroup(Group $group): static
    {
        if ($this->groups->contains($group)) {
            $this->groups->removeElement($group);
            $group->removeUser($this);
        }
        return $this;
    }

    /**
     * @param string $name Name or shortname of the group to be checked
     * @return bool
     */
    public function hasGroup(string $name): bool
    {
        foreach ($this->groups as $group) {
            if ($group->getShortname() == $name || $group->getName() == $name) {
                return true;
            }
        }
        return false;
    }

    /**
     * @return Collection<int,Address>
     */
    public function getAddresses(): Collection
    {
        return $this->addresses;
    }

    public function addAddress(Address $address): static
    {
        if (!$this->addresses->contains($address)) {
            $this->addresses[] = $address;
            $address->setUser($this);
        }
        return $this;
    }

    public function removeAddress(Address $address): static
    {
        if ($this->addresses->contains($address)) {
            $this->addresses->removeElement($address);
            if ($address->getUser() === $this) {
                $address->setUser(null);
            }
        }
        return $this;
    }

    public function getProvince(): ?Province
    {
        return $this->province;
    }

    public function setProvince(?Province $province): static
    {
        $this->province = $province;
        return $this;
    }

    public function getCountry(): ?Country
    {
        return $this->country;
    }

    public function setCountry(?Country $country): static
    {
        $this->country = $country;
        return $this;
    }

    public static function getSortableProps(): array
    {
        return ['username', 'lastname', 'enabled', 'created'];
    }

    public static function getBundleName(): string
    {
        return 'Auth';
    }
}
