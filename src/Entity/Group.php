<?php

/**
 * This file is part of the Allmega Auth Bundle package.
 *
 * @copyright Allmega 
 * @package   Auth Bundle
 * @author    Eduard Jung <eddie@allmega.de>
 * @license   https://opensource.org/licenses/gpl-license.php GNU Public License
 */

namespace Allmega\AuthBundle\Entity;

use Allmega\BlogBundle\Utils\Helper;
use Allmega\AuthBundle\Repository\GroupRepository;
use Allmega\BlogBundle\Model\SortableItemInterface;
use Allmega\AuthBundle\Model\{BaseProps, GroupInterface};
use Doctrine\Common\Collections\{ArrayCollection, Collection};
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: GroupRepository::class)]
#[ORM\Table(name: '`allmega_auth__group`')]
class Group extends BaseProps implements GroupInterface, SortableItemInterface
{
    #[ORM\ManyToMany(targetEntity: Role::class)]
    #[ORM\JoinTable(name: '`allmega_auth__group_role`')]
    #[ORM\OrderBy(['name' => 'ASC'])]
    private Collection $roles;

    #[ORM\ManyToMany(targetEntity: User::class, inversedBy: 'groups')]
    #[ORM\JoinTable(name: '`allmega_auth__group_user`')]
    #[ORM\OrderBy(['lastname' => 'ASC'])]
    private Collection $users;

    #[ORM\ManyToMany(targetEntity: GroupType::class, inversedBy: 'groups')]
    #[ORM\JoinTable(name: '`allmega_auth__group_grouptype`')]
    #[ORM\OrderBy(['name' => 'ASC'])]
    private Collection $grouptypes;
    
    /**
     * Create a new Group entity with predetermined data, 
     * if no data is provided, it will be generated:
     * - $shortname, name as dummy text
     * - $roles, array with Role entities
     */
    public static function build(
        GroupType $grouptype = null,
        string $shortname = null,
        array $roles = [],
        string $name = null,
        string $description = null): static
    {
        $shortname = $shortname ?? Helper::generateRandomString();
        $name = $name ?? Helper::generateRandomString();

        $group = (new static())
            ->setDescription($description)
            ->setShortname($shortname)
            ->setName($name);

        if ($grouptype) $group->addGrouptype($grouptype);
        foreach ($roles as $role) $group->addRole($role);

        return $group;
    }

    public function __construct()
    {
        $this->roles = new ArrayCollection();
        $this->users = new ArrayCollection();
        $this->grouptypes = new ArrayCollection();
    }

    /**
     * @return Collection<int,Role>
     */
    public function getRoles(): Collection
    {
        return $this->roles;
    }

    public function addRole(Role $role): static
    {
        if (!$this->roles->contains($role)) $this->roles[] = $role;
        return $this;
    }

    public function removeRole(Role $role): static
    {
        if ($this->roles->contains($role)) $this->roles->removeElement($role);
        return $this;
    }

    public function getRolenames(): array
    {
        $roleNames = [];
        foreach ($this->getRoles() as $role) $roleNames[] = $role->getName();
        return $roleNames;
    }

    public function hasRole(string $name): bool
    {
        return in_array($name, $this->getRolenames());
    }

    /**
     * @return Collection<int,User>
     */
    public function getUsers(): Collection
    {
        return $this->users;
    }

    public function addUser(User $user): static
    {
        if (!$this->users->contains($user)) $this->users[] = $user;
        return $this;
    }

    public function removeUser(User $user): static
    {
        if ($this->users->contains($user)) $this->users->removeElement($user);
        return $this;
    }

    /**
     * @return Collection<int,GroupType>
     */
    public function getGrouptypes(): Collection
    {
        return $this->grouptypes;
    }

    public function addGrouptype(GroupType $grouptype): static
    {
        if (!$this->grouptypes->contains($grouptype)) $this->grouptypes[] = $grouptype;
        return $this;
    }

    public function removeGrouptype(GroupType $grouptype): static
    {
        if ($this->grouptypes->contains($grouptype)) $this->grouptypes->removeElement($grouptype);
        return $this;
    }

    public static function getSortableProps(): array
    {
        return ['name'];
    }

    public static function getBundleName(): string
    {
        return 'Auth';
    }
}