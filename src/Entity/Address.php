<?php

/**
 * This file is part of the Allmega Auth Bundle package.
 *
 * @copyright Allmega 
 * @package   Auth Bundle
 * @author    Eduard Jung <eddie@allmega.de>
 * @license   https://opensource.org/licenses/gpl-license.php GNU Public License
 */

namespace Allmega\AuthBundle\Entity;

use Allmega\BlogBundle\Utils\{Helper, IdGenerator};
use Allmega\AuthBundle\Repository\AddressRepository;
use Random\RandomException;
use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\DBAL\Types\Types;

#[ORM\Entity(repositoryClass: AddressRepository::class)]
#[ORM\Table(name: '`allmega_auth__address`')]
class Address
{
    #[ORM\Id]
    #[ORM\Column(length: 191)]
    #[ORM\GeneratedValue(strategy: 'CUSTOM')]
    #[ORM\CustomIdGenerator(class: IdGenerator::class)]
    private ?string $id = null;

    #[ORM\Column(length: 191)]
    #[Assert\NotBlank(message: 'errors.blank')]
    #[Assert\Length(max: 191, maxMessage: 'errors.max_value')]
    private ?string $street = null;

    #[ORM\Column(length: 191, nullable: true)]
    #[Assert\Length(max: 191, maxMessage: 'errors.max_value')]
    private ?string $description = null;

    #[ORM\Column(type: Types::INTEGER)]
    #[Assert\NotBlank(message: 'errors.blank')]
    private ?int $housenumber =  null;

    #[ORM\Column(type: Types::INTEGER)]
    #[Assert\NotBlank(message: 'errors.blank')]
    private ?int $zip = null;

    #[ORM\Column(length: 191)]
    #[Assert\NotBlank(message: 'errors.blank')]
    #[Assert\Length(max: 191, maxMessage: 'errors.max_value')]
    private ?string $city = null;

    #[ORM\ManyToOne(inversedBy: 'addresses')]
    #[ORM\JoinColumn(nullable: false)]
    private ?User $user;

    /**
     * Create a new Address entity with predetermined data,
     * if no data is provided, it will be generated:
     * - $description, $street, $city as dummy text
     * - $user will be created
     *
     * @throws RandomException
     */
    public static function build(
        string $description = null,
        int $housenumber = null,
        string $street = null,
        User $user = null,
        string $city = null,
        int $zip = null): static
    {
        $street = $street ?? Helper::generateRandomString();
        $housenumber = $housenumber ?? random_int(1,100);
        $city = $city ?? Helper::generateRandomString();
        $zip = $zip ?? random_int(10000,99999);
        $user = $user ?? User::build();

        return (new static())
            ->setDescription($description)
            ->setHousenumber($housenumber)
            ->setStreet($street)
            ->setCity($city)
            ->setUser($user)
            ->setZip($zip);
    }

    public function getId(): ?string
    {
        return $this->id;
    }

    public function getStreet(): ?string
    {
        return $this->street;
    }

    public function setStreet(string $street): static
    {
        $this->street = $street;
        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(?string $description): static
    {
        $this->description = $description;
        return $this;
    }

    public function getHousenumber(): ?int
    {
        return $this->housenumber;
    }

    public function setHousenumber(int $housenumber): static
    {
        $this->housenumber = $housenumber;
        return $this;
    }

    public function getZip(): ?int
    {
        return $this->zip;
    }

    public function setZip(int $zip): static
    {
        $this->zip = $zip;
        return $this;
    }

    public function getCity(): ?string
    {
        return $this->city;
    }

    public function setCity(string $city): static
    {
        $this->city = $city;
        return $this;
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setUser(?User $user): static
    {
        $this->user = $user;
        return $this;
    }
}