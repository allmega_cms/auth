<?php

/**
 * This file is part of the Allmega Auth Bundle package.
 *
 * @copyright Allmega 
 * @package   Auth Bundle
 * @author    Eduard Jung <eddie@allmega.de>
 * @license   https://opensource.org/licenses/gpl-license.php GNU Public License
 */

namespace Allmega\AuthBundle\Entity;

use Allmega\BlogBundle\Utils\Helper;
use Allmega\AuthBundle\Model\BaseProps;
use Allmega\AuthBundle\Repository\RoleRepository;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: RoleRepository::class)]
#[ORM\Table(name: '`allmega_auth__role`')]
class Role extends BaseProps
{      
    /**
     * Create a new Role entity with predetermined data, 
     * if no data is provided, it will be generated:
     * - $name, $shortname as dummy text
     */
    public static function build(
        string $name = null,
        string $description = null,
        string $shortname = null): static
    {
        $shortname = $shortname ?? Helper::generateRandomString();
        $name = $name ?? Helper::generateRandomString();

        return (new static())
            ->setDescription($description)
            ->setShortname($shortname)
            ->setName($name);
    }
}