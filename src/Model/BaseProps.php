<?php

/**
 * This file is part of the Allmega Auth Bundle package.
 *
 * @copyright Allmega 
 * @package   Auth Bundle
 * @author    Eduard Jung <eddie@allmega.de>
 * @license   https://opensource.org/licenses/gpl-license.php GNU Public License
 */

namespace Allmega\AuthBundle\Model;

use Allmega\BlogBundle\Utils\IdGenerator;
use Allmega\BlogBundle\Model\SortableItemInterface;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\DBAL\Types\Types;

#[UniqueEntity(['name'], message: 'errors.exists')]
class BaseProps implements SortableItemInterface
{
    #[ORM\Id]
    #[ORM\Column(length: 191)]
    #[ORM\GeneratedValue(strategy: 'CUSTOM')]
    #[ORM\CustomIdGenerator(class: IdGenerator::class)]
    protected ?string $id = null;

    #[ORM\Column(length: 191, unique: true)]
    #[Assert\NotBlank(message: 'errors.blank')]
    #[Assert\Length(max: 191, maxMessage: 'errors.max_value')]
    protected ?string $name = null;

    #[ORM\Column(length: 191, nullable: true)]
    #[Assert\Length(max: 191, maxMessage: 'errors.max_value')]
    protected ?string $shortname = null;

    #[ORM\Column(length: 191, nullable: true)]
    #[Assert\Length(max: 191, maxMessage: 'errors.max_value')]
    protected ?string $description = null;

    #[ORM\Column(type: Types::BOOLEAN)]
    protected bool $selectable = false;

    #[ORM\Column(type: Types::BOOLEAN)]
    protected bool $sys = false;

    public function getId(): ?string
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): static
    {
        $this->name = $name;
        return $this;
    }

    public function getShortname(): ?string
    {
        return $this->shortname;
    }

    public function setShortname(?string $shortname): static
    {
        $this->shortname = $shortname;
        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(?string $description): static
    {
        $this->description = $description;
        return $this;
    }

	public function isSelectable(): bool
    {
		return $this->selectable;
	}

	public function setSelectable(bool $selectable): static
    {
		$this->selectable = $selectable;
		return $this;
	}

    public function isSys(): bool
    {
        return $this->sys;
    }

    public function setSys(bool $sys): static
    {
        $this->sys = $sys;
        return $this;
    }

    public static function getSortableProps(): array
    {
        return ['name', 'description', 'sys'];
    }

    public static function getBundleName(): string
    {
        return 'Auth';
    }
}