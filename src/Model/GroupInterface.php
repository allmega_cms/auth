<?php

/**
 * This file is part of the Allmega Auth Bundle package.
 *
 * @copyright Allmega 
 * @package   Auth Bundle
 * @author    Eduard Jung <eddie@allmega.de>
 * @license   https://opensource.org/licenses/gpl-license.php GNU Public License
 */

namespace Allmega\AuthBundle\Model;

use Doctrine\Common\Collections\Collection;

interface GroupInterface
{
    public function getName(): ?string;
    public function getRoles(): Collection;
}