<?php

/**
 * This file is part of the Allmega Auth Bundle package.
 *
 * @copyright Allmega
 * @package   Auth Bundle
 * @author    Eduard Jung <eddie@allmega.de>
 * @license   https://opensource.org/licenses/gpl-license.php GNU Public License
 */

namespace Allmega\AuthBundle\Model;

use Allmega\AuthBundle\Entity\Group;

trait GroupsTrait
{
    private function getGroupsOptions(): array
    {
        if (!$this->groupsParams->getQueryBuilder()) $this->setGroupsQueryBuilder();

        return array_merge(
            $this->groupsParams->getBaseOptions(Group::class, 'name'),
            $this->groupsParams->getMultipleOptions(),
        );
    }

    private function setGroupsQueryBuilder(): void
    {
        $shortnames = $this->groupsParams->getShortnames();
        $queryBuilder = $this->groupRepo->findByGroupTypesShortnames($shortnames);
        $this->groupsParams->setQueryBuilder($queryBuilder);
    }
}