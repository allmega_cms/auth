<?php

/**
 * This file is part of the Allmega Auth Bundle package.
 *
 * @copyright Allmega
 * @package   Auth Bundle
 * @author    Eduard Jung <eddie@allmega.de>
 * @license   https://opensource.org/licenses/gpl-license.php GNU Public License
 */

namespace Allmega\AuthBundle\Model;

use Allmega\AuthBundle\Entity\User;

trait UsersTrait
{
    private function getUsersOptions(): array
    {
        if (!$this->usersParams->getQueryBuilder()) $this->setUsersQueryBuilder();

        return array_merge(
            $this->usersParams->getBaseOptions(User::class, 'fullname'),
            $this->usersParams->getMultipleOptions(),
        );
    }

    private function setUsersQueryBuilder(): void
    {
        $roles = $this->usersParams->getRoles();
        $queryBuilder = $this->userRepo->findByRolesQueryBuilder($roles);
        $this->usersParams->setQueryBuilder($queryBuilder);
    }
}