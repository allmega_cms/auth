<?php

/**
 * This file is part of the Allmega Auth Bundle package.
 *
 * @copyright Allmega 
 * @package   Auth Bundle
 * @author    Eduard Jung <eddie@allmega.de>
 * @license   https://opensource.org/licenses/gpl-license.php GNU Public License
 */

namespace Allmega\AuthBundle\Model;

use Allmega\AuthBundle\Entity\Group;
use Doctrine\Common\Collections\Collection;

interface GroupableInterface
{
    public function getGroups(): Collection;
    public function addGroup(Group $group): static;
    public function removeGroup(Group $group): static;
}