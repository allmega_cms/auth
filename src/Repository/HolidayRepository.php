<?php

/**
 * This file is part of the Allmega Auth Bundle package.
 *
 * @copyright Allmega 
 * @package   Auth Bundle
 * @author    Eduard Jung <eddie@allmega.de>
 * @license   https://opensource.org/licenses/gpl-license.php GNU Public License
 */

namespace Allmega\AuthBundle\Repository;

use Allmega\AuthBundle\Entity\Holiday;
use Doctrine\Persistence\ManagerRegistry;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;

class HolidayRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Holiday::class);
    }
}
