<?php

/**
 * This file is part of the Allmega Auth Bundle package.
 *
 * @copyright Allmega 
 * @package   Auth Bundle
 * @author    Eduard Jung <eddie@allmega.de>
 * @license   https://opensource.org/licenses/gpl-license.php GNU Public License
 */

namespace Allmega\AuthBundle\Repository;

use Allmega\AuthBundle\Data;
use Doctrine\ORM\QueryBuilder;
use Allmega\AuthBundle\Entity\{Group, User};
use Allmega\BlogBundle\Model\SearchableInterface;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;
use Doctrine\ORM\Query;

class GroupRepository extends ServiceEntityRepository implements SearchableInterface
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Group::class);
    }

    public function findAllQuery(): Query
    {
        return $this->createQueryBuilder('a')->orderBy('a.name', 'ASC')->getQuery();
    }

    public function findNotEqualToGroupId(string $gid): array
    {
        return $this->createQueryBuilder('g')
            ->where('g.id <> :gid')
            ->setParameter('gid', $gid)
            ->getQuery()
            ->getResult();
    }

    public function findByShortNames(array $groups): array
    {
        return $this->createQueryBuilder('g')
            ->where('g.shortname IN (:groups)')
            ->setParameter('groups', $groups)
            ->getQuery()
            ->getResult();
    }

    public function findByGroupTypesShortnames(array $shortnames): QueryBuilder
    {
        return $this
            ->createQueryBuilder('g')
            ->join('g.grouptypes', 't')
            ->where("t.shortname IN (:shortnames)")
            ->setParameter('shortnames', $shortnames)
            ->andWhere('g.selectable = 1')
            ->orderBy('g.name', 'ASC');
    }

    public function findByNames(array $groups): array
    {
        return $this->createQueryBuilder('g')
            ->where('g.name IN (:groups)')
            ->setParameter('groups', $groups)
            ->getQuery()
            ->getResult();
    }

    public function findDepartments(): array
    {
        return $this->createQueryBuilder('g')
            ->join('g.grouptypes', 't')
            ->where("t.shortname = :shortname")
            ->setParameter('shortname', Data::GROUP_TYPE_DEPARTMENT)
            ->orderBy('g.name', 'ASC')
            ->getQuery()
            ->getResult();
    }

    public function findDepartmentsByUserId(User $user): array
    {
        return $this->createQueryBuilder('g')
            ->join('g.grouptypes', 't')
            ->join('g.users', 'u')
            ->where("t.shortname = :shortname")
            ->setParameter('shortname', Data::GROUP_TYPE_DEPARTMENT)
            ->andWhere('u.id = :id')
            ->orderBy('g.name', 'ASC')
            ->setParameter('id', $user->getId())
            ->getQuery()
            ->getResult();
    }

    public function findByIds(array $ids): array
    {
        return $this->createQueryBuilder('g')
            ->where('g.id IN (:ids)')
            ->setParameter('ids', $ids)
            ->getQuery()
            ->getResult();
    }

    public function findBySearchQuery(array $terms, array $options, int $limit): array
    {
        if (0 === count($terms)) return [];

        $query = $this->createQueryBuilder('g');

        foreach ($terms as $key => $term) {
            $query->orWhere('g.name LIKE :n_'.$key)->setParameter('n_'.$key, '%'.$term.'%');
            $query->orWhere('g.description LIKE :d_'.$key)->setParameter('d_'.$key, '%'.$term.'%');
        }

        return $query
            ->orderBy('g.name', 'ASC')
            ->setMaxResults($limit)
            ->getQuery()
            ->getResult();
    }
}