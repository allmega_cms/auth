<?php

/**
 * This file is part of the Allmega Auth Bundle package.
 *
 * @copyright Allmega 
 * @package   Auth Bundle
 * @author    Eduard Jung <eddie@allmega.de>
 * @license   https://opensource.org/licenses/gpl-license.php GNU Public License
 */

namespace Allmega\AuthBundle\Repository;

use Allmega\AuthBundle\Entity\GroupType;
use Doctrine\Persistence\ManagerRegistry;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;

class GroupTypeRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, GroupType::class);
    }

    public function findByShortnames(array $shortnames): array
    {
        return $this->createQueryBuilder('g')
            ->where('g.shortname IN (:shortnames)')
            ->setParameter('shortnames', $shortnames)
            ->getQuery()->getResult();
    }
}
