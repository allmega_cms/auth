<?php

/**
 * This file is part of the Allmega Auth Bundle package.
 *
 * @copyright Allmega 
 * @package   Auth Bundle
 * @author    Eduard Jung <eddie@allmega.de>
 * @license   https://opensource.org/licenses/gpl-license.php GNU Public License
 */

namespace Allmega\AuthBundle\Repository;

use Allmega\AuthBundle\Entity\User;
use Allmega\BlogBundle\Model\SearchableInterface;
use Symfony\Bridge\Doctrine\Security\User\UserLoaderInterface;
use Symfony\Component\Security\Core\User\PasswordUpgraderInterface;
use Symfony\Component\Security\Core\Exception\UnsupportedUserException;
use Symfony\Component\Security\Core\User\PasswordAuthenticatedUserInterface;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\{EntityNotFoundException, QueryBuilder};
use Doctrine\Persistence\ManagerRegistry;
use Doctrine\ORM\Query\ResultSetMapping;
use Doctrine\ORM\Query;
use DateInterval;
use DateTime;

class UserRepository extends ServiceEntityRepository implements UserLoaderInterface, PasswordUpgraderInterface, SearchableInterface
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, User::class);
    }

    public function upgradePassword(PasswordAuthenticatedUserInterface $user, string $newHashedPassword): void
    {
        $user->setPassword($newHashedPassword);
        $this->getEntityManager()->persist($user);
        $this->getEntityManager()->flush();
    }

    public function findOnline(): array
    {
        return $this->createQueryBuilder('u')
            ->where('u.lastActivity IS NOT NULL')
            ->andWhere('u.lastActivity BETWEEN :last AND :now')
            ->setParameter('now', (new DateTime())->add(new DateInterval('PT2M')))
            ->setParameter('last', (new DateTime())->sub(new DateInterval('PT5M')))
            ->orderBy('u.lastActivity', 'DESC')
            ->getQuery()
            ->getResult();
    }

    public function loadUserByIdentifier(string $identifier): ?User
    {
        $this->setExpiredInactiv();

        $user = $this->createQueryBuilder('u')
            ->where('u.username = :username')
            ->andWhere('u.enabled = 1')
            ->andWhere('u.deleted = 0')
            ->andWhere('u.verified = 1')
            ->andWhere('u.expires > :now OR u.expires IS NULL')
            ->setParameter('username', $identifier)
            ->setParameter('now', new DateTime())
            ->setMaxResults(1)
            ->getQuery()
            ->getOneOrNullResult();

        if ($user) {
            $user->setLastLogin()->setLastSeen($user->getLastActivity());
            $this->getEntityManager()->persist($user);
            $this->getEntityManager()->flush();
        }
        return $user;
    }

    public function setExpiredInactiv(): void
    {
        $this->createQueryBuilder('u')
            ->update()
            ->set('u.enabled', 0)
            ->where('u.expires < :now')
            ->setParameter('now', new DateTime())
            ->getQuery()
            ->execute();
    }

    public function findByLastLogged(int $limit = 5): array
    {
        return $this->createQueryBuilder('u')
            ->orderBy('u.lastLogin', 'DESC')
            ->setMaxResults($limit)
            ->getQuery()
            ->getResult();
    }

    public function findAllQuery(): Query
    {
        return $this->createQueryBuilder('a')
            ->where('a.deleted=0')
            ->orderBy('a.lastname', 'ASC')
            ->getQuery();
    }

    public function findByGroupIdsQuery(array $gids, bool $enabled = false): Query
    {
        $query = $this->createQueryBuilder('a')
            ->join('a.groups', 'g')
            ->where('g.id  IN (:gids)')
            ->orderBy('a.lastname', 'ASC')
            ->setParameter('gids', $gids);
        
        if ($enabled) $query->andWhere('a.enabled=1');
        return $query->getQuery();
    }

    public function findByGroupIds(array $gids): array
    {
        return $this->findByGroupIdsQuery($gids)->getResult();
    }

    public function findByGroupId(string $gid): array
    {
        return $this->createQueryBuilder('u')
            ->join('u.groups', 'g')
            ->where('g.id = :gid')
            ->orderBy('u.lastname', 'ASC')
            ->setParameter('gid', $gid)
            ->getQuery()
            ->getResult();
    }

    private function createQueryBuilderByRoles(array $roles): QueryBuilder
    {
        return $this->createQueryBuilder('u')
            ->leftJoin('u.permissions', 'p')
            ->leftJoin('u.groups', 'g')
            ->join('g.roles', 'r')
            ->where('r.name IN (:roles)')
            ->orWhere('p.name IN (:roles)')
            ->setParameter('roles', $roles);
    }

    public function findByRolesQuery(array $roles, bool $enabled = true, int $limit = 0): Query
    {
        $queryBuilder = $this->createQueryBuilderByRoles($roles);

        if ($enabled) $queryBuilder->andWhere('u.enabled=1');
        if ($limit > 0) $queryBuilder->setMaxResults($limit);

        return $queryBuilder->getQuery();
    }

    public function findByRolesQueryBuilder(array $roles): QueryBuilder
    {
        return $this->createQueryBuilderByRoles($roles)
            ->andWhere('u.enabled=1')
            ->orderBy('u.lastname', 'ASC');
    }

    public function findByRoles(array $roles, bool $enabled = true, int $limit = 0): array
    {
        return $this->findByRolesQuery($roles, $enabled, $limit)->getResult();
    }

    public function findOneByRole(string $role): ?User
    {
        return $this->findByRolesQuery([$role], true, 1)->getOneOrNullResult();
    }

    public function deleteUserRelations(string $table, User $user): void
    {
        $em = $this->getEntityManager();
        $rsm = new ResultSetMapping();

        $em->createNativeQuery('SET FOREIGN_KEY_CHECKS = 0', $rsm)->execute();

        $sql = "DELETE FROM `$table` WHERE user_id = :id";
        $em->createNativeQuery($sql, $rsm)
            ->setParameter('id', $user->getId())
            ->execute();
    }

    public function updateField(string $class, string $fieldName, User $user): static
    {
        $item = $this->findOneBy(['username' => 'ghost']);
        if (!$item) {
            throw new EntityNotFoundException('Error!!! User with ghost username was not found!');
        }

        $fieldName = 'u.' . $fieldName;

        $this
            ->getEntityManager()
            ->getRepository($class)
            ->createQueryBuilder('u')
            ->update()
            ->set($fieldName, ':id')
            ->where($fieldName . ' = :uid')
            ->setParameter('uid', $user->getId())
            ->setParameter('id', $item->getId())
            ->getQuery()->execute();

        return $this;
    }

    public function findBySearchQuery(array $terms, array $options, int $limit): array
    {
        if (0 === count($terms)) return [];

        $query = $this->createQueryBuilder('c');

        foreach ($terms as $key => $term) {
            $query->orWhere('c.firstname LIKE :f_'.$key)->setParameter('f_'.$key, '%'.$term.'%');
            $query->orWhere('c.lastname LIKE :l_'.$key)->setParameter('l_'.$key, '%'.$term.'%');
            $query->orWhere('c.phone LIKE :p_'.$key)->setParameter('p_'.$key, '%'.$term.'%');
            $query->orWhere('c.mobile LIKE :m_'.$key)->setParameter('m_'.$key, '%'.$term.'%');
            $query->orWhere('c.email LIKE :e_'.$key)->setParameter('e_'.$key, '%'.$term.'%');
        }

        return $query
            ->orderBy('c.lastname', 'ASC')
            ->setMaxResults($limit)
            ->getQuery()
            ->getResult();
    }
}