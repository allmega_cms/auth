<?php

/**
 * This file is part of the Allmega Auth Bundle package.
 *
 * @copyright Allmega 
 * @package   Auth Bundle
 * @author    Eduard Jung <eddie@allmega.de>
 * @license   https://opensource.org/licenses/gpl-license.php GNU Public License
 */

namespace Allmega\AuthBundle\Repository;

use Allmega\AuthBundle\Entity\Role;
use Doctrine\Persistence\ManagerRegistry;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;

class RoleRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Role::class);
    }

    public function findByNames(array $names): array
    {
        return $this->createQueryBuilder('r')
            ->where('r.name IN (:names)')
            ->setParameter('names', $names)
            ->getQuery()->getResult();
    }

    public function findByShortNames(array $shortnames): array
    {
        return $this->createQueryBuilder('r')
            ->where('r.shortname IN (:shortnames)')
            ->setParameter('shortnames', $shortnames)
            ->getQuery()
            ->getResult();
    }
}
