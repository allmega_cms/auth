<?php

/**
 * This file is part of the Allmega Auth Bundle package.
 *
 * @copyright Allmega 
 * @package   Auth Bundle
 * @author    Eduard Jung <eddie@de>
 * @license   https://opensource.org/licenses/gpl-license.php GNU Public License
 */

namespace Allmega\AuthBundle;

final class Events
{
    public const PROVINCE_CREATED = 'province.created';
    public const PROVINCE_UPDATED = 'province.updated';
    public const PROVINCE_DELETED = 'province.deleted';
    public const PROVINCE_STATE_CHANGED = 'province.state_changed';

    public const GROUPTYPE_CREATED = 'grouptype.created';
    public const GROUPTYPE_UPDATED = 'grouptype.updated';
    public const GROUPTYPE_DELETED = 'grouptype.deleted';
    public const GROUPTYPE_DELETE_FAILED = 'grouptype.delete_failed';

    public const GROUP_CREATED = 'group.created';
    public const GROUP_UPDATED = 'group.updated';
    public const GROUP_DELETED = 'group.deleted';
    public const GROUP_DELETE_FAILED = 'group.delete_failed';

    public const USER_CREATED = 'user.created';
    public const USER_UPDATED = 'user.updated';
    public const USER_DELETE = 'user.delete_event';
    public const USER_DELETED = 'user.deleted';
    public const USER_REGISTERED = 'user.registered';
    public const USER_STATE_CHANGED = 'user.state_changed';
    public const USER_VERIFIED_CHANGED = 'user.verified_changed';
    public const USER_PASSWORD_CHANGED = 'user.password_changed';

    public const REGISTRATION_COMPLETED = 'registration.completed';

    public const ADDRESS_CREATED = 'address.created';
    public const ADDRESS_UPDATED = 'address.updated';
    public const ADDRESS_DELETED = 'address.deleted';
}