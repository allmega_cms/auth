<?php

/**
 * This file is part of the Allmega Auth Bundle package.
 *
 * @copyright Allmega 
 * @package   Auth Bundle
 * @author    Eduard Jung <eddie@allmega.de>
 * @license   https://opensource.org/licenses/gpl-license.php GNU Public License
 */

namespace Allmega\AuthBundle\EventSubscriber;

use Allmega\AuthBundle\Events;
use Allmega\BlogBundle\Model\FlashesTrait;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

class FlashSubscriber implements EventSubscriberInterface
{
    use FlashesTrait;

    public static function getSubscribedEvents(): array
    {
        return [
            Events::ADDRESS_CREATED => 'addSuccessFlash',
            Events::ADDRESS_UPDATED => 'addSuccessFlash',
            Events::ADDRESS_DELETED => 'addSuccessFlash',
            Events::GROUPTYPE_CREATED => 'addSuccessFlash',
            Events::GROUPTYPE_UPDATED => 'addSuccessFlash',
            Events::GROUPTYPE_DELETED => 'addSuccessFlash',
            Events::GROUPTYPE_DELETE_FAILED => 'addWarningFlash',
            Events::GROUP_CREATED => 'addSuccessFlash',
            Events::GROUP_UPDATED => 'addSuccessFlash',
            Events::GROUP_DELETED => 'addSuccessFlash',
            Events::GROUP_DELETE_FAILED => 'addWarningFlash',
            Events::USER_CREATED => 'addSuccessFlash',
            Events::USER_UPDATED => 'addSuccessFlash',
            Events::USER_DELETED => 'addSuccessFlash',
            Events::USER_REGISTERED => 'addSuccessFlash',
            Events::USER_STATE_CHANGED => 'addSuccessFlash',
            Events::USER_VERIFIED_CHANGED => 'addSuccessFlash',
            Events::USER_PASSWORD_CHANGED => 'addSuccessFlash',
            Events::REGISTRATION_COMPLETED => 'addSuccessFlash',
            Events::PROVINCE_STATE_CHANGED => 'addSuccessFlash',
            Events::PROVINCE_UPDATED => 'addSuccessFlash',
        ];
    }
}