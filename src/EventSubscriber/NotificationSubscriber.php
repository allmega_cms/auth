<?php

/**
 * This file is part of the Allmega Auth Bundle package.
 *
 * @copyright Allmega 
 * @package   Auth Bundle
 * @author    Eduard Jung <eddie@allmega.de>
 * @license   https://opensource.org/licenses/gpl-license.php GNU Public License
 */

namespace Allmega\AuthBundle\EventSubscriber;

use Allmega\AuthBundle\{Data, Events};
use Allmega\BlogBundle\Entity\Notification;
use Allmega\AuthBundle\Security\EmailVerifier;
use Allmega\BlogBundle\Utils\Params\NotificationParams;
use Allmega\AuthBundle\Controller\{RegistrationController, UserController};
use Symfony\Component\EventDispatcher\{EventSubscriberInterface, GenericEvent};
use Symfony\Component\Mailer\Exception\TransportExceptionInterface;
use Symfony\Component\Messenger\Exception\ExceptionInterface;
use Symfony\Component\Security\Http\Event\LoginFailureEvent;
use Symfony\Contracts\Translation\TranslatorInterface;
use Symfony\Component\Messenger\MessageBusInterface;
use Symfony\Bridge\Twig\Mime\TemplatedEmail;
use Symfony\Component\Mime\Address;
use DateTime;

readonly class NotificationSubscriber implements EventSubscriberInterface
{
    public function __construct(
        private TranslatorInterface $translator,
        private EmailVerifier $emailVerifier,
        private MessageBusInterface $bus,
        private string $senderName,
        private string $sender) {}

    public static function getSubscribedEvents(): array
    {
        return [
            LoginFailureEvent::class => 'onFailedLogin',
            Events::REGISTRATION_COMPLETED => 'onRegistrationCompleted',
            Events::USER_REGISTERED => 'onUserRegistered',
            Events::USER_CREATED => 'onUserCreated',
        ];
    }

    /**
     * @throws ExceptionInterface
     */
    public function onUserCreated(GenericEvent $event): void
    {
        $user = $event->getSubject();
        $params = ['user' => $user, 'password' => $event->getArgument('password')];
        $subject = $this->translator->trans('registration.email.title', [], Data::DOMAIN);

        $notification = (new Notification())
            ->setTemplate(UserController::ROUTE_TEMPLATE_PATH . 'inc/_email.html.twig')
            ->setReceivers([$user->getEmail()])
            ->setSubject($subject)
            ->setParams($params);

        $this->bus->dispatch($notification);
    }

    /**
     * @throws TransportExceptionInterface
     */
    public function onUserRegistered(GenericEvent $event): void
    {
        $subject = $this->translator->trans('registration.email.confirm', [], Data::DOMAIN);
        $template = RegistrationController::ROUTE_TEMPLATE_PATH . 'inc/_email.html.twig';
        $routeName = RegistrationController::ROUTE_NAME . 'verify_email';
        $user = $event->getSubject();

        $this->emailVerifier->sendEmailConfirmation($routeName, $user,
            (new TemplatedEmail())
                ->from(new Address($this->sender, $this->senderName))
                ->to($user->getEmail())
                ->subject($subject)
                ->htmlTemplate($template)
        );
    }

    /**
     * @throws ExceptionInterface
     */
    public function onRegistrationCompleted(GenericEvent $event): void
    {
        $user = $event->getSubject();
        $subject = $this->translator->trans('registration.email.title', [], Data::DOMAIN);
        $params = ['user' => $user, 'password' => '*************'];

        $notification = (new Notification())
            ->setTemplate(UserController::ROUTE_TEMPLATE_PATH . 'inc/_email.html.twig')
            ->setReceivers([$user->getEmail()])
            ->setSubject($subject)
            ->setParams($params);

        $this->bus->dispatch($notification);
    }

    /**
     * @throws ExceptionInterface
     */
    public function onFailedLogin(LoginFailureEvent $event): void
    {
        $now = (new DateTime())->format('d.m.Y H:i');
        $request = $event->getRequest();

        $search = ['%username%', '%ip_address%','%event_date%'];
        $replace = [
            $request->request->get('_username'),
            $request->getClientIp(),
            $now,
        ];

        $params = new NotificationParams(Data::NOTIFICATION_LOGIN_FAILED, [], $search, $replace);
        $this->bus->dispatch($params);
    }
}