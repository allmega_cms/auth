<?php

/**
 * This file is part of the Allmega Auth Bundle package.
 *
 * @copyright Allmega 
 * @package   Auth Bundle
 * @author    Eduard Jung <eddie@allmega.de>
 * @license   https://opensource.org/licenses/gpl-license.php GNU Public License
 */

namespace Allmega\AuthBundle;

use Allmega\AuthBundle\Controller\{GroupController, GroupTypeController, MemberController, ProfileController, ProvinceController, UserController};
use Allmega\BlogBundle\Utils\Register\Entries\{ControllerEntriesMap, ControllerEntry};
use Allmega\AuthBundle\Entity\{Country, Holiday, Province, User};
use Allmega\BlogBundle\Utils\Loader\Entries\MenupointEntry;
use Allmega\BlogBundle\Model\PackageData;
use Allmega\BlogBundle\Data as BlogData;

class Data extends PackageData
{
    public const DOMAIN = 'AllmegaAuthBundle';
    public const PACKAGE = 'auth';

    public const NOTIFICATION_LOGIN_FAILED = 'auth_login_failed';

    public const GROUP_TYPE_ADMINISTRATION = 'auth.administration';
    public const GROUP_TYPE_DEPARTMENT = 'auth.department';

    public const DEPARTMENT_EMPLOYEE_GROUP = 'auth.department.employee';
    public const DEPARTMENT_HEAD_GROUP = 'auth.department.head';

    public const OAUTH_GROUP = 'auth.oauth';
    public const ADMIN_GROUP = 'auth.admin';
    public const USER_GROUP = 'auth.user';

    public const DEPARTMENT_EMPLOYEE_ROLE = 'ROLE_DEPARTMENT_EMPLOYEE';
    public const DEPARTMENT_HEAD_ROLE = 'ROLE_DEPARTMENT_HEAD';
    public const OAUTH_USER_ROLE = 'ROLE_OAUTH_USER';
    public const ADMIN_ROLE = 'ROLE_ADMIN';
    public const USER_ROLE = 'ROLE_USER';

    protected function setRegisterData(): void
    {
        $this->package = self::PACKAGE;
        $this->data = [
            'controllerEntries' => $this->getControllerEntries(),
            'envEntries' => $this->getEnvEntries(),
        ];
    }

    protected function setLoadData(): void
    {
        $this->package = self::PACKAGE;
        $this->data = [
            'notificationtypes' => $this->getNotificationTypes(),
            'menupoints' => $this->getMenuPoints(),
            'grouptypes' => $this->getGroupTypes(),
            'countries' => $this->getCountries(),
            'provinces' => $this->getProvinces(),
            'holidays' => $this->getHolidays(),
            'groups' => $this->getGroups(),
            'roles' => $this->getRoles(),
            'users' => $this->getUsers(),
        ];
    }

    /**
     * @return array<int,ControllerEntry>
     */
    protected function getControllerEntries(): array
    {
        return [
            new ControllerEntry(
                ControllerEntriesMap::DASHBOARD_BOXES,
                self::PACKAGE,
                'profile',
                ProfileController::class,
                'getDashboardWidget'),
            new ControllerEntry(
                ControllerEntriesMap::DASHBOARD_BOXES,
                self::PACKAGE,
                'lastlogin',
                UserController::class,
                'getDashboardWidget'),
            new ControllerEntry(
                ControllerEntriesMap::PROFILE_MENU,
                self::PACKAGE,
                'profile',
                ProfileController::class,
                'getMenuIcon'),
            new ControllerEntry(
                ControllerEntriesMap::POPUP_BOXES,
                self::PACKAGE,
                'online',
                MemberController::class,
                'getSidePopupWidget'),
        ];
    }

    protected function getEnvEntriesData(): array
    {
        return [
            ['LOGIN_FAILED_BLOCK', 'How long, in minutes, should login attempts be blocked after logging in incorrectly 3 times? [{default}]', 5],
            ['USERS_ENABLED', 'Please enter how many employees can be created or registered? [{default}]: ', 0],
            ['REGISTRATION_ENABLED', 'Should the registration process be allowed? [{default}]', 0],
            ['USERS_DELETE', 'Should the users deletion be allowed? [{default}]', 0],
        ];
    }

    /**
     * @return array<int,User>
     */
    protected function getUsers(): array
    {
        $groups = $this->buildGroupsForUsers();
        $users = [
            User::build('admin', 'admin', 'Alex', 'Hofmann', 'alex@home.lan', $groups, true, true),
            User::build('ghost', 'ghost', 'Max', 'Mustermann', 'max@home.lan', [], false, false, true),
        ];
        return [self::PACKAGE => $users];
    }

    /**
     * @return array<int,MenupointEntry>
     */
    protected function getMenuPoints(): array
    {
        $users = [self::USER_GROUP];
        $admins = [self::ADMIN_GROUP];

        $main = 'auth.main';

        $menuPoints = [
            new MenupointEntry('auth.employees', MemberController::ROUTE_NAME . 'index', [], $this->routeType, 1, $users, $main),
            new MenupointEntry('auth.provinces', ProvinceController::ROUTE_NAME . 'index', [], $this->routeType, 2, $admins, $main),
            new MenupointEntry('auth.grouptypes', GroupTypeController::ROUTE_NAME . 'index', [], $this->routeType, 3, $admins, $main),
            new MenupointEntry('auth.groups', GroupController::ROUTE_NAME . 'index', [], $this->routeType, 4, $admins, $main),
            new MenupointEntry('auth.users', UserController::ROUTE_NAME . 'index', [], $this->routeType, 5, $admins, $main),
            new MenupointEntry($main, '', [], $this->menuType, 3, $users, BlogData::MENUPOINT_BLOG_MANAGE),
        ];
        return [self::PACKAGE => $this->setSysAndActive($menuPoints)];
    }

    /**
     * @return array<int,Holiday>
     */
    protected function getHolidays(): array
    {
        $data = [
            'corpus_christi' => ['Baden-Württemberg', 'Bayern', 'Hessen', 'Nordrhein-Westfalen', 'Rheinland-Pfalz', 'Saarland'],
            'all_saints_day' => ['Baden-Württemberg', 'Bayern', 'Nordrhein-Westfalen', 'Rheinland-Pfalz', 'Saarland'],
            'reformation_day' => ['Brandenburg', 'Mecklenburg-Vorpommern', 'Sachsen', 'Sachsen-Anhalt', 'Thüringen'],
            'holy_three_kings' => ['Baden-Württemberg', 'Bayern', 'Sachsen-Anhalt'],
            'assumption_day' => ['Bayern', 'Saarland'],
            'repentance_day' => ['Sachsen'],
            'sunday' => ['Brandenburg'],
        ];

        $provinces = [];
        foreach ($data as $key => $provincesNames) {
            foreach ($provincesNames as $name) $provinces[$key][] = Province::build(name: $name);
        }

        extract($provinces);

        $holidays = [
            Holiday::build('holy_three_kings', 'Heilige Drei Könige', $holy_three_kings),
            Holiday::build('reformation_day', 'Reformationstag', $reformation_day),
            Holiday::build('assumption_day', 'Mariä Himmelfahrt', $assumption_day),
            Holiday::build('repentance_day', 'Buß- und Bettag', $repentance_day),
            Holiday::build('all_saints_day', 'Allerheiligen', $all_saints_day),
            Holiday::build('corpus_christi', 'Fronleichnam', $corpus_christi),
            Holiday::build('german_unity_day', 'Tag der Deutschen Einheit'),
            Holiday::build('second_christmasday', 'Zweiter Weihnachtstag'),
            Holiday::build('first_christmasday', 'Erster Weihnachtstag'),
            Holiday::build('ascension_christ', 'Christi Himmelfahrt'),
            Holiday::build('easter_sunday', 'Ostersonntag', $sunday),
            Holiday::build('whit_sunday', 'Pfingstsonntag', $sunday),
            Holiday::build('ash_wednesday', 'Aschermittwoch'),
            Holiday::build('working_day', 'Tag der Arbeit'),
            Holiday::build('easter_monday', 'Ostermontag'),
            Holiday::build('whit_monday', 'Pfingstmontag'),
            Holiday::build('christmas_eve', 'Heiligabend'),
            Holiday::build('roses_monday', 'Rosenmontag'),
            Holiday::build('good_friday', 'Karfreitag'),
            Holiday::build('sylvester', 'Silvester'),
            Holiday::build('new_year', 'Neujahr'),
        ];
        return [self::PACKAGE => $this->setSysAndSelectable($holidays)];
    }

    /**
     * @return array<int,Country>
     */
    protected function getCountries(): array
    {
        return [self::PACKAGE => $this->setSysAndSelectable([Country::build('de')])];
    }

    /**
     * @return array<int,Province>
     */
    protected function getProvinces(): array
    {
        $provinces = [];
        $names = [
            'Baden-Württemberg',
            'Bayern',
            'Berlin',
            'Brandenburg',
            'Bremen',
            'Hamburg',
            'Hessen',
            'Mecklenburg-Vorpommern',
            'Niedersachsen',
            'Nordrhein-Westfalen',
            'Rheinland-Pfalz',
            'Saarland',
            'Sachsen',
            'Sachsen-Anhalt',
            'Schleswig-Holstein',
            'Thüringen'
        ];

        foreach ($names as $name) $provinces[] = Province::build(name: $name);
        return [self::PACKAGE => $this->setSysAndSelectable($provinces)];
    }

    protected function getAuthData(): array
    {
        return [
            self::GROUP_TYPE_ADMINISTRATION => [
                self::ADMIN_GROUP => [
                    self::ADMIN_ROLE => 'auth.admin',
                    self::USER_ROLE => 'auth.user',
                ],
                self::USER_GROUP => [
                    self::USER_ROLE => 'auth.user',
                ],
                self::OAUTH_GROUP => [
                    self::OAUTH_USER_ROLE => 'auth.oauth',
                ],
            ],
            self::GROUP_TYPE_DEPARTMENT => [
                self::DEPARTMENT_EMPLOYEE_GROUP => [
                    self::DEPARTMENT_EMPLOYEE_ROLE => 'auth.department.employee',
                ],
                self::DEPARTMENT_HEAD_GROUP => [
                    self::DEPARTMENT_EMPLOYEE_ROLE => 'auth.department.employee',
                    self::DEPARTMENT_HEAD_ROLE => 'auth.department.head',
                ],
            ],
        ];
    }

    protected function getNotificationTypesData(): array
    {
        return [
            self::NOTIFICATION_LOGIN_FAILED,
        ];
    }
}