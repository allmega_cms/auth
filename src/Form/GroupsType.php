<?php

/**
 * This file is part of the Allmega Auth Bundle package.
 *
 * @copyright Allmega 
 * @package   Auth Bundle
 * @author    Eduard Jung <eddie@allmega.de>
 * @license   https://opensource.org/licenses/gpl-license.php GNU Public License
 */

namespace Allmega\AuthBundle\Form;

use Allmega\AuthBundle\Data;
use Allmega\BlogBundle\Data as BlogData;
use Allmega\AuthBundle\Entity\{Group, GroupType};
use Symfony\Component\Form\{FormBuilderInterface, AbstractType};
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Contracts\Translation\TranslatorInterface;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Doctrine\ORM\EntityRepository;

class GroupsType extends AbstractType
{
    public function __construct(private readonly TranslatorInterface $translator) {}

    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('name', null, [
                'label' => 'grouptype.label.name',
                'attr' => ['autofocus' => true, 'placeholder' => 'grouptype.label.name']
            ])
            ->add('description', null, [
                'label' => 'grouptype.label.description',
                'attr' => ['placeholder' => 'grouptype.label.description']
            ])
            ->add('groups', EntityType::class, [
                'label' => 'grouptype.label.groups',
                'class' => Group::class,
                'choice_label'  => 'name',
                'query_builder' => function (EntityRepository $er) {
                    return $er->createQueryBuilder('r')->orderBy('r.name', 'ASC');
                },
                'multiple' => true,
                'required' => false,
                'by_reference' => false,
                'help' => 'grouptype.help.groups',
                'help_translation_parameters' => [
                    '%multiplemark%' => $this->translator->trans('action.multiplemark', [], BlogData::DOMAIN)
                ],
                'help_html' => true
            ]);
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => GroupType::class,
            'translation_domain' => Data::DOMAIN
        ]);
    }
}