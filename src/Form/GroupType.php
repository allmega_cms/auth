<?php

/**
 * This file is part of the Allmega Auth Bundle package.
 *
 * @copyright Allmega 
 * @package   Auth Bundle
 * @author    Eduard Jung <eddie@allmega.de>
 * @license   https://opensource.org/licenses/gpl-license.php GNU Public License
 */

namespace Allmega\AuthBundle\Form;

use Allmega\AuthBundle\Data;
use Allmega\BlogBundle\Data as BlogData;
use Allmega\AuthBundle\Entity\{Role, User, Group, GroupType as GroupsType};
use Symfony\Component\Form\{FormBuilderInterface, AbstractType};
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Contracts\Translation\TranslatorInterface;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Doctrine\ORM\EntityRepository;

class GroupType extends AbstractType
{
    public function __construct(private readonly TranslatorInterface $translator) {}

    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $multiplemark = $this->translator->trans('action.multiplemark', [], BlogData::DOMAIN);
        $builder
            ->add('name', null, [
                'attr' => ['autofocus' => true, 'placeholder' => 'group.name'],
                'label' => 'group.name'
            ])
            ->add('description', null, [
                'attr' => ['placeholder' => 'label.description'],
                'label' => 'label.description'
            ])
            ->add('roles', EntityType::class, [
                'label' => 'label.roles',
                'class' => Role::class,
                'choice_label'  => 'name',
                'query_builder' => function (EntityRepository $er) {
                    return $er->createQueryBuilder('r')->orderBy('r.name', 'ASC');
                },
                'multiple' => true,
                'required' => false,
                'help' => 'group.help.roles',
                'help_translation_parameters' => ['%multiplemark%' => $multiplemark],
                'help_html' => true
            ])
            ->add('users', EntityType::class, [
                'label' => 'group.users',
                'class' => User::class,
                'choice_label' => 'fullname',
                'query_builder' => function (EntityRepository $er) {
                    return $er->createQueryBuilder('u')
                        ->where('u.enabled=1')
                        ->orderBy('u.lastname', 'ASC');
                },
                'multiple' => true,
                'required' => false,
                'help' => 'group.help.users',
                'help_translation_parameters' => ['%multiplemark%' => $multiplemark],
                'help_html' => true
            ])
            ->add('grouptypes', EntityType::class, [
                'label' => 'group.grouptypes',
                'class' => GroupsType::class,
                'choice_label' => 'name',
                'query_builder' => function (EntityRepository $er) {
                    return $er->createQueryBuilder('g')->orderBy('g.name', 'ASC');
                },
                'multiple' => true,
                'required' => false,
                'help' => 'group.help.grouptypes',
                'help_translation_parameters' => ['%multiplemark%' => $multiplemark],
                'help_html' => true
            ])
            ->add('selectable', null, [
                'label' => 'group.label.selectable',
                'help' => 'group.help.selectable'
            ]);
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Group::class,
            'translation_domain' => Data::DOMAIN
        ]);
    }
}