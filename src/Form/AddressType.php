<?php

/**
 * This file is part of the Allmega Auth Bundle package.
 *
 * @copyright Allmega 
 * @package   Auth Bundle
 * @author    Eduard Jung <eddie@allmega.de>
 * @license   https://opensource.org/licenses/gpl-license.php GNU Public License
 */

namespace Allmega\AuthBundle\Form;

use Allmega\AuthBundle\Data;
use Allmega\AuthBundle\Entity\Address;
use Symfony\Component\Form\{FormBuilderInterface, AbstractType};
use Symfony\Component\OptionsResolver\OptionsResolver;

class AddressType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('street', null, [
                'label' => 'address.label.street',
                'attr' => ['autofocus' => true, 'placeholder' => 'address.label.street']
            ])
            ->add('housenumber', null, [
                'label' => 'address.label.housenumber',
                'attr' => ['placeholder' => 'address.label.housenumber']
            ])
            ->add('zip', null, [
                'label' => 'address.label.zip',
                'attr' => ['placeholder' => 'address.label.zip']
            ])
            ->add('city', null, [
                'label' => 'address.label.city',
                'attr' => ['placeholder' => 'address.label.city']
            ])
            ->add('description', null, [
                'label' => 'label.description',
                'attr' => ['placeholder' => 'label.description']
            ]);
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Address::class,
            'translation_domain' => Data::DOMAIN
        ]);
    }
}