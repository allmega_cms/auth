<?php

/**
 * This file is part of the Allmega Auth Bundle package.
 *
 * @copyright Allmega 
 * @package   Auth Bundle
 * @author    Eduard Jung <eddie@allmega.de>
 * @license   https://opensource.org/licenses/gpl-license.php GNU Public License
 */

namespace Allmega\AuthBundle\Form;

use Allmega\AuthBundle\Data;
use Allmega\AuthBundle\Entity\User;
use Allmega\BlogBundle\Form\Type\DatePickerType;
use Symfony\Component\Form\{FormBuilderInterface, AbstractType};
use Symfony\Component\OptionsResolver\OptionsResolver;

class ProfileType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('username', null, [
                'label' => 'user.label.username',
                'attr' => ['autofocus' => true, 'placeholder' => 'user.label.username']
            ])
            ->add('firstname', null, [
                'label' => 'user.label.firstname',
                'attr' => ['placeholder' => 'user.label.firstname']
            ])
            ->add('lastname', null, [
                'label' => 'user.label.lastname',
                'attr' => ['placeholder' => 'user.label.lastname']
            ])
            ->add('email', null, [
                'label' => 'user.label.email',
                'attr' => ['placeholder' => 'user.label.email']
            ])
            ->add('phone', null, [
                'label' => 'user.label.phone',
                'attr' => ['placeholder' => 'user.label.phone']
            ])
            ->add('mobile', null, [
                'label' => 'user.label.mobile',
                'attr' => ['placeholder' => 'user.label.mobile']
            ])
            ->add('internally', null, [
                'label' => 'user.label.internally',
                'attr' => ['placeholder' => 'user.label.internally']
            ])
            ->add('birthday', DatePickerType::class, [
                'label' => 'user.label.birthday',
                'help' => 'user.help.birthday',
                'required' => false
            ]);
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => User::class,
            'translation_domain' => Data::DOMAIN
        ]);
    }
}