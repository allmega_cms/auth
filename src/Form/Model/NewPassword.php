<?php

/**
 * This file is part of the Allmega Auth Bundle package.
 *
 * @copyright Allmega 
 * @package   Auth Bundle
 * @author    Eduard Jung <eddie@allmega.de>
 * @license   https://opensource.org/licenses/gpl-license.php GNU Public License
 */

namespace Allmega\AuthBundle\Form\Model;

use Symfony\Component\Validator\Constraints as Assert;

class NewPassword
{
	private string $newPassword;

	public function getNewPassword(): ?string
	{
		return $this->newPassword;
	}

	public function setNewPassword(string $newPassword): static
	{
		$this->newPassword = $newPassword;
		return $this;
	}
}