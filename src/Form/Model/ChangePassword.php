<?php

/**
 * This file is part of the Allmega Auth Bundle package.
 *
 * @copyright Allmega 
 * @package   Auth Bundle
 * @author    Eduard Jung <eddie@allmega.de>
 * @license   https://opensource.org/licenses/gpl-license.php GNU Public License
 */

namespace Allmega\AuthBundle\Form\Model;

use Symfony\Component\Security\Core\Validator\Constraints as SecurityAssert;

class ChangePassword extends NewPassword
{
	#[SecurityAssert\UserPassword(message: 'errors.password_mismatch')]
	private string $oldPassword;

	public function getOldPassword(): ?string
	{
		return $this->oldPassword;
	}

	public function setOldPassword(string $oldPassword): static
	{
		$this->oldPassword = $oldPassword;
		return $this;
	}
}