<?php

/**
 * This file is part of the Allmega Auth Bundle package.
 *
 * @copyright Allmega 
 * @package   Auth Bundle
 * @author    Eduard Jung <eddie@allmega.de>
 * @license   https://opensource.org/licenses/gpl-license.php GNU Public License
 */

namespace Allmega\AuthBundle\Form;

use Allmega\AuthBundle\Data;
use Allmega\BlogBundle\Data as BlogData;
use Allmega\AuthBundle\Entity\{Province, Holiday};
use Symfony\Component\Form\{FormBuilderInterface, AbstractType};
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Contracts\Translation\TranslatorInterface;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Doctrine\ORM\EntityRepository;

class ProvinceType extends AbstractType
{
    public function __construct(private readonly TranslatorInterface $translator) {}

    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('name', null, [
                'label' => 'province.label.name',
                'attr' => ['autofocus' => true, 'placeholder' => 'province.label.name']
            ])
            ->add('description', null, [
                'attr' => ['placeholder' => 'label.description'],
                'label' => 'label.description'
            ])
            ->add('holidays', EntityType::class, [
                'label' => 'province.label.holidays',
                'multiple' => true,
                'class' => Holiday::class,
                'choice_label' => 'name',
                'query_builder' => function (EntityRepository $er) {
                    return $er->createQueryBuilder('h')->orderBy('h.name', 'ASC');
                },
                'help' => 'province.help.holidays',
                'help_translation_parameters' => [
                    '%multiplemark%' => $this->translator->trans('action.multiplemark', [], BlogData::DOMAIN)
                ],
                'help_html' => true
            ])
            ->add('active', null, ['label' => 'province.label.active']);
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Province::class,
            'translation_domain' => Data::DOMAIN
        ]);
    }
}