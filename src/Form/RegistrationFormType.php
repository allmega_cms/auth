<?php

/**
 * This file is part of the Allmega Auth Bundle package.
 *
 * @copyright Allmega
 * @package   Auth Bundle
 * @author    Eduard Jung <eddie@allmega.de>
 * @license   https://opensource.org/licenses/gpl-license.php GNU Public License
 */

namespace Allmega\AuthBundle\Form;

use Allmega\AuthBundle\Data;
use Allmega\BlogBundle\Utils\Helper;
use Allmega\AuthBundle\Entity\{Country, Province, User};
use Symfony\Component\Form\Extension\Core\Type\{CheckboxType, RepeatedType};
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Routing\RouterInterface;
use Symfony\Component\Form\AbstractType;
use Doctrine\ORM\EntityRepository;

class RegistrationFormType extends AbstractType
{
    public function __construct(
        private readonly RouterInterface $router,
        private readonly string $termsPageSlug) {}

    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $url = $this->router->generate('allmega_blog_page_show', ['slug' => $this->termsPageSlug]);
        $pwParams = array_merge(Helper::getRepeatPasswordInputOptions(), ['mapped' => false]);

        $builder
            ->add('username', null, [
                'label' => 'user.label.username',
                'attr' => ['autofocus' => true, 'placeholder' => 'user.label.username']
            ])
            ->add('firstname', null, [
                'label' => 'user.label.firstname',
                'attr' => ['placeholder' => 'user.label.firstname']
            ])
            ->add('lastname', null, [
                'label' => 'user.label.lastname',
                'attr' => ['placeholder' => 'user.label.lastname']
            ])
            ->add('email', null, [
                'label' => 'user.label.email',
                'attr' => ['placeholder' => 'user.label.email']
            ])
            ->add('phone', null, [
                'label' => 'user.label.phone',
                'attr' => ['placeholder' => 'user.label.phone']
            ])
            ->add('country', EntityType::class, [
                'label' => 'user.label.country',
                'help' => 'user.help.country',
                'class' => Country::class,
                'choice_label' => 'name',
                'query_builder' => function (EntityRepository $er) {
                    return $er->createQueryBuilder('c')
                        ->where('c.selectable=1')
                        ->orderBy('c.name', 'ASC');
                }
            ])
            ->add('province', EntityType::class, [
                'label' => 'user.label.province',
                'help' => 'user.help.province',
                'class' => Province::class,
                'choice_label' => 'name',
                'query_builder' => function (EntityRepository $er) {
                    return $er->createQueryBuilder('p')
                        ->where('p.active=1')
                        ->orderBy('p.name', 'ASC');
                }
            ])
            ->add('newPassword', RepeatedType::class, $pwParams)
            ->add('agreeTerms', CheckboxType::class, Helper::getTermsInputOptions($url));
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => User::class,
            'translation_domain' => Data::DOMAIN,
        ]);
    }
}