<?php

/**
 * This file is part of the Allmega Auth Bundle package.
 *
 * @copyright Allmega 
 * @package   Auth Bundle
 * @author    Eduard Jung <eddie@allmega.de>
 * @license   https://opensource.org/licenses/gpl-license.php GNU Public License
 */

namespace Allmega\AuthBundle\Form;

use Allmega\AuthBundle\Data;
use Allmega\BlogBundle\Data as BlogData;
use Allmega\BlogBundle\Form\Type\DatePickerType;
use Allmega\AuthBundle\Entity\{Country, User, Group, Province, Role};
use Symfony\Component\Form\{FormBuilderInterface, AbstractType};
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Contracts\Translation\TranslatorInterface;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Doctrine\ORM\EntityRepository;

class UserType extends AbstractType
{
    public function __construct(private readonly TranslatorInterface $translator) {}

    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('country', EntityType::class, [
                'label' => 'user.label.country',
                'help' => 'user.help.country',
                'class' => Country::class,
                'choice_label' => 'name',
                'query_builder' => function (EntityRepository $er) {
                    return $er->createQueryBuilder('c')
                        ->where('c.selectable=1')
                        ->orderBy('c.name', 'ASC');
                }
            ])
            ->add('province', EntityType::class, [
                'label' => 'user.label.province',
                'help' => 'user.help.province',
                'class' => Province::class,
                'choice_label' => 'name',
                'query_builder' => function (EntityRepository $er) {
                    return $er->createQueryBuilder('p')
                        ->where('p.active=1')
                        ->orderBy('p.name', 'ASC');
                }
            ])
            ->add('permissions', EntityType::class, [
                'label' => 'user.label.permissions',
                'class' => Role::class,
                'choice_label' => 'name',
                'query_builder' => function (EntityRepository $er) {
                    return $er->createQueryBuilder('r')->orderBy('r.name', 'ASC');
                },
                'multiple' => true,
                'required' => false,
                'help' => 'user.help.permissions',
                'help_html' => true,
                'help_translation_parameters' => [
                    '%multiplemark%' => $this->getMultipleMark()
                ]
            ])
            ->add('groups', EntityType::class, [
                'label' => 'user.label.groups',
                'class' => Group::class,
                'choice_label' => 'name',
                'query_builder' => function (EntityRepository $er) {
                    return $er->createQueryBuilder('g')->orderBy('g.name', 'ASC');
                },
                'multiple' => true,
                'required' => false,
                'by_reference' => false,
                'help' => 'user.help.groups',
                'help_html' => true,
                'help_translation_parameters' => [
                    '%multiplemark%' => $this->getMultipleMark()
                ]
            ])
            ->add('expires', DatePickerType::class, [
                'label' => 'user.label.expires',
                'help' => 'user.help.expires',
                'required' => false
            ])
            ->add('enabled', null, [
                'label' => 'user.label.enabled',
                'help' => 'user.help.enabled'
            ])
            ->add('verified', null, [
                'label' => 'user.label.verified'
            ])
            ->add('selectable', null, [
                'label' => 'user.label.selectable',
                'help' => 'user.help.selectable'
            ]);
    }

    public function getParent(): ?string
    {
        return ProfileType::class;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => User::class,
            'translation_domain' => Data::DOMAIN
        ]);
    }

    private function getMultipleMark(): string
    {
        return $this->translator->trans('action.multiplemark', [], BlogData::DOMAIN);
    }
}