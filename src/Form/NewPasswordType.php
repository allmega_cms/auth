<?php

/**
 * This file is part of the Allmega Auth Bundle package.
 *
 * @copyright Allmega 
 * @package   Auth Bundle
 * @author    Eduard Jung <eddie@allmega.de>
 * @license   https://opensource.org/licenses/gpl-license.php GNU Public License
 */

namespace Allmega\AuthBundle\Form;

use Allmega\AuthBundle\Data;
use Allmega\BlogBundle\Utils\Helper;
use Allmega\AuthBundle\Form\Model\NewPassword;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\{FormBuilderInterface, AbstractType};
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;

class NewPasswordType extends AbstractType
{
	public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder->add('newPassword', RepeatedType::class, Helper::getRepeatPasswordInputOptions());
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => NewPassword::class,
            'translation_domain' => Data::DOMAIN
        ]);
    }
}