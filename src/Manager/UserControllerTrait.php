<?php

/**
 * This file is part of the Allmega Auth Bundle package.
 *
 * @copyright Allmega 
 * @package   Blog Bundle
 * @author    Eduard Jung <eddie@allmega.de>
 * @license   https://opensource.org/licenses/gpl-license.php GNU Public License
 */

namespace Allmega\AuthBundle\Manager;

use Allmega\AuthBundle\Entity\User;
use Allmega\BlogBundle\Utils\Helper;
use Allmega\AuthBundle\Form\UserType;
use Allmega\AuthBundle\{Data, Events};
use Allmega\AuthBundle\Controller\UserController;
use Allmega\BlogBundle\Utils\Params\BaseControllerParams;
use Symfony\Component\HttpFoundation\Response;

trait UserControllerTrait
{
    use ChangePasswordTrait;

    protected function preExecution(): static
    {
        $user = $this->params->getEntity();
        switch ($this->params->getAction()) {
            case $this->add:
                $this->checkUserCapacity() ? $this->handlePassword($user) : $this->cancel();
                break;
            case $this->edit:
                $user->setUpdated();
                break;
            case $this->delete:
                $user->getPermissions()->clear();
                $user->getGroups()->clear();
                break;
            default:
        }
        return $this;
    }

    protected function postExecution(): static
    {
        switch ($this->params->getAction()) {
            case $this->delete: 
                $this->params->setEventName(Events::USER_DELETE);
                $this->dispatchEvent();
                break;
            default:
        }
        return $this;
    }

    protected function setRedirect(): static
    {
        switch ($this->params->getAction()) {
            case $this->changePassword:
            case $this->state:
            case $this->edit:
            case $this->add:
                $this->params
                    ->setRouteName($this->params->getRouteShort(), $this->show)
                    ->addRouteParams(['id' => $this->params->getEntity()->getId()]);
                break;
            default:
        }
        return $this;
    }

    private function handlePassword(User &$user): void
    {
        $passwordHasher = $this->services->getService('passwordHasher');
        $password = Helper::generateRandomString(9);
        $user->setPassword($passwordHasher->hashPassword($user, $password));
        $this->params->addArguments(['password' => $password]);
    }

    private function changeUserPassword(User $user): Response
    {
        $params = (new BaseControllerParams())->init(
            formParams: $this->buildFormParams($user, false),
            entity: $user,
            domain: Data::DOMAIN,
            eventName: Events::USER_PASSWORD_CHANGED,
            routeName: UserController::ROUTE_NAME,
            templatesPath: UserController::ROUTE_TEMPLATE_PATH
        );
        return $this->handle($params, $this->changepw);
    }

    private function save(User $user = null, array $arguments = []): Response
    {
        $options = $user ? [] : ['usersEnabled' => $this->usersEnabled];
        $formParams = $this->buildFormParams($user);

        $eventName = $user ? Events::USER_UPDATED : Events::USER_CREATED;
        $user = $user ?? new User();

        $params = (new BaseControllerParams())->init(
            arguments: $arguments,
            formParams: $formParams,
            options: $options,
            entity: $user,
            domain: Data::DOMAIN,
            eventName: $eventName,
            formType: UserType::class,
            routeName: UserController::ROUTE_NAME,
            templatesPath: UserController::ROUTE_TEMPLATE_PATH
        );
        return $this->handle($params);
    }

    private function buildFormParams(?User $user, bool $save = true): array
    {
        $params = [];
        if ($save) {
            if ($user) {
                $params = [
                    'item' => $user,
                    'showDelete' => $this->usersDelete,
                    'link' => [
                        'route' => UserController::ROUTE_NAME . 'show',
                        'params' => ['id' => $user->getId()],
                        'title' => 'user.show'
                    ]
                ];
            }
        } else {
            $params = [
                'title' => ['content' => 'user.change_password'],
                'item' => $user,
                'link' => [
                    'params' => ['id' => $user->getId()],
                    'route' => UserController::ROUTE_NAME . 'show',
                    'title' => 'user.show'
                ]
            ];
        }
        return $params;
    }

    private function checkUserCapacity(): bool
    {
        $options = $this->params->getOptions();
        if (array_key_exists('usersEnabled', $options) && $options['usersEnabled'] > 0) {
            $count = $options['usersEnabled'];
            $countExists = $this->getEntityManager()->getRepository(User::class)->count();
            if ($count >= $countExists) {
                $this->params->addErrorMessage('user.to_many');
                return false;
            }
        }
        return true;
    }
}