<?php

/**
 * This file is part of the Allmega Auth Bundle package.
 *
 * @copyright Allmega 
 * @package   Blog Bundle
 * @author    Eduard Jung <eddie@allmega.de>
 * @license   https://opensource.org/licenses/gpl-license.php GNU Public License
 */

namespace Allmega\AuthBundle\Manager;

use Allmega\AuthBundle\{Data, Events};
use Allmega\AuthBundle\Entity\Province;
use Allmega\AuthBundle\Form\ProvinceType;
use Allmega\AuthBundle\Controller\ProvinceController;
use Allmega\BlogBundle\Utils\Params\BaseControllerParams;
use Symfony\Component\HttpFoundation\Response;

trait ProvinceControllerTrait
{
    private function save(Province $province = null, array $arguments = []): Response
    {
        $eventName = $province ? Events::PROVINCE_UPDATED : Events::PROVINCE_CREATED;
        $province = $province ?? new Province();

        $params = (new BaseControllerParams())->init(
            arguments: $arguments,
            formParams: ['item' => $province],
            entity: $province,
            domain: Data::DOMAIN,
            eventName: $eventName,
            formType: ProvinceType::class,
            routeName: ProvinceController::ROUTE_NAME,
            templatesPath: ProvinceController::ROUTE_TEMPLATE_PATH
        );
        return $this->handle($params);
    }
}