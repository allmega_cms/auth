<?php

/**
 * This file is part of the Allmega Auth Bundle package.
 *
 * @copyright Allmega 
 * @package   Blog Bundle
 * @author    Eduard Jung <eddie@allmega.de>
 * @license   https://opensource.org/licenses/gpl-license.php GNU Public License
 */

namespace Allmega\AuthBundle\Manager;

use Allmega\AuthBundle\{Data, Events};
use Allmega\AuthBundle\Form\GroupsType;
use Allmega\AuthBundle\Entity\GroupType;
use Allmega\AuthBundle\Controller\GroupTypeController;
use Allmega\BlogBundle\Utils\Params\BaseControllerParams;
use Symfony\Component\HttpFoundation\Response;

trait GrouptypeControllerTrait
{
    protected function preExecution(): static
    {
        switch ($this->params->getAction()) {
            case $this->delete:
                if ($this->params->getEntity()->isSys()) {
                    $this->params->setEventName(Events::GROUPTYPE_DELETE_FAILED);
                    $this->cancel();
                } else {
                    $this->params->getEntity()->getGroups()->clear();
                }
                break;
            default:
        }
        return $this;
    }

    private function save(GroupType $grouptype = null, array $arguments = []): Response
    {
        $eventName = $grouptype ? Events::GROUPTYPE_UPDATED : Events::GROUPTYPE_CREATED;
        $grouptype = $grouptype ?? new GroupType();

        $params = (new BaseControllerParams())->init(
            arguments: $arguments,
            formParams: ['item' => $grouptype],
            entity: $grouptype,
            domain: Data::DOMAIN,
            eventName: $eventName,
            formType: GroupsType::class,
            routeName: GroupTypeController::ROUTE_NAME,
            templatesPath: GroupTypeController::ROUTE_TEMPLATE_PATH
        );
        return $this->handle($params);
    }
}