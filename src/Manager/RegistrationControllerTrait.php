<?php

/**
 * This file is part of the Allmega Auth Bundle package.
 *
 * @copyright Allmega
 * @package   Blog Bundle
 * @author    Eduard Jung <eddie@allmega.de>
 * @license   https://opensource.org/licenses/gpl-license.php GNU Public License
 */

namespace Allmega\AuthBundle\Manager;

use Allmega\AuthBundle\Entity\User;
use Allmega\AuthBundle\Entity\Role;
use Allmega\AuthBundle\{Data, Events};
use Allmega\AuthBundle\Form\RegistrationFormType;
use Allmega\AuthBundle\Controller\SecurityController;
use Allmega\BlogBundle\Utils\Params\BaseControllerParams;
use Symfony\Component\HttpFoundation\Response;

trait RegistrationControllerTrait
{
    protected function preExecution(): static
    {
        switch ($this->params->getAction()) {
            case $this->add:
                if ($this->checkUserEmail()) {
                    $this
                        ->setUserPassword()
                        ->setUserExpires()
                        ->addUserRole();
                }
                break;
            default:
        }
        return $this;
    }

    protected function setRedirect(): static
    {
        switch ($this->params->getAction()) {
            case $this->add:
                $this->params->setRouteName(SecurityController::ROUTE_NAME, 'login');
                break;
            default:
        }
        return $this;
    }

    private function checkUserEmail(): bool
    {
        $sender = $this->params->getEntity()->getEmail();
        $result = $this->validateEmails([$sender]);

        if (!$result[$sender]) {
            $this->params->addErrorMessage('label.invalid_email');
            $this->cancel();
            return false;
        }
        return true;
    }

    private function setUserPassword(): static
    {
        $plainPassword = $this->form->get('newPassword')->getData();
        $hashedPassword = $this->services
            ->getService('passwordHasher')
            ->hashPassword($this->params->getEntity(), $plainPassword);

        $this->params->getEntity()->setPassword($hashedPassword);
        return $this;
    }

    private function setUserExpires(): static
    {
        $now = new \DateTime();
        $this->params
            ->getEntity()
            ->setExpires($now)
            ->setEnabled(true);

        return $this;
    }

    private function addUserRole(): void
    {
        $permission = $this
            ->getEntityManager()
            ->getRepository(Role::class)
            ->findOneBy(['name' => Data::USER_ROLE]);

        $this->params->getEntity()->addPermission($permission);
    }

    private function save(): Response
    {
        $params = (new BaseControllerParams())->init(
            hideLinks: true,
            entity: new User(),
            domain: Data::DOMAIN,
            eventName: Events::USER_REGISTERED,
            formPath: self::ROUTE_TEMPLATE_PATH,
            formType: RegistrationFormType::class,
            templatesPath: self::ROUTE_TEMPLATE_PATH,
        )->setRouteName(self::ROUTE_NAME, $this->add);
        return $this->handle($params);
    }
}