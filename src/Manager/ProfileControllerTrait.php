<?php

/**
 * This file is part of the Allmega Auth Bundle package.
 *
 * @copyright Allmega 
 * @package   Blog Bundle
 * @author    Eduard Jung <eddie@allmega.de>
 * @license   https://opensource.org/licenses/gpl-license.php GNU Public License
 */

namespace Allmega\AuthBundle\Manager;

use Allmega\AuthBundle\{Data, Events};
use Allmega\AuthBundle\Form\ProfileType;
use Allmega\AuthBundle\Controller\ProfileController;
use Allmega\BlogBundle\Utils\Params\BaseControllerParams;
use Symfony\Component\HttpFoundation\Response;

trait ProfileControllerTrait
{
    use ChangePasswordTrait;

    private function save(): Response
    {
        $params = (new BaseControllerParams())->init(
            formParams: $this->buildFormParams(),
            entity: $this->getUser(),
            domain: Data::DOMAIN,
            eventName: Events::USER_UPDATED,
            formType: ProfileType::class,
            prop: 'profile',
            templatesPath: ProfileController::ROUTE_TEMPLATE_PATH
        )->setRouteName(ProfileController::ROUTE_NAME, $this->show);
        return $this->handle($params);
    }

    private function changeProfilePassword(): Response
    {
        $params = (new BaseControllerParams())->init(
            formParams: $this->buildFormParams(false),
            entity: $this->getUser(),
            domain: Data::DOMAIN,
            eventName: Events::USER_PASSWORD_CHANGED,
            prop: 'profile',
            templatesPath: ProfileController::ROUTE_TEMPLATE_PATH
        )->setRouteName(ProfileController::ROUTE_NAME, $this->show);
        return $this->handle($params, $this->changepw);
    }

    private function buildFormParams(bool $save = true): array
    {
        $title = $save ? [] : ['content' => 'profile.change_password'];
        return [
            'title' => $title,
            'link' => [
                'route' => ProfileController::ROUTE_NAME . 'show',
                'title' => 'profile.show'
            ]
        ];
    }
}