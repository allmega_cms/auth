<?php

/**
 * This file is part of the Allmega Auth Bundle package.
 *
 * @copyright Allmega 
 * @package   Blog Bundle
 * @author    Eduard Jung <eddie@allmega.de>
 * @license   https://opensource.org/licenses/gpl-license.php GNU Public License
 */

namespace Allmega\AuthBundle\Manager;

use Allmega\AuthBundle\{Data, Events};
use Allmega\AuthBundle\Entity\Address;
use Allmega\AuthBundle\Form\AddressType;
use Allmega\BlogBundle\Utils\Params\BaseControllerParams;
use Allmega\AuthBundle\Controller\{AddressController, ProfileController};
use Symfony\Component\HttpFoundation\Response;

trait AddressControllerTrait
{
    protected function setRedirect(): static
    {
        $this->params->setRouteName(ProfileController::ROUTE_NAME, $this->show);
        return $this;
    }

    private function save(Address $address = null, array $arguments = []): Response
    {
        $formParams = $this->buildFormParams($address);

        $eventName = $address ? Events::ADDRESS_UPDATED : Events::ADDRESS_CREATED;
        $address = $address ?? (new Address())->setUser($this->getUser());

        $params = (new BaseControllerParams())->init(
            arguments: $arguments,
            formParams: $formParams,
            entity: $address,
            domain: Data::DOMAIN,
            eventName: $eventName,
            formType: AddressType::class,
            routeName: AddressController::ROUTE_NAME,
            templatesPath: AddressController::ROUTE_TEMPLATE_PATH
        );
        return $this->handle($params);
    }

    private function buildFormParams(?Address $address): array
    {
        return [
            'item' => $address,
            'link' => [
                'route' => ProfileController::ROUTE_NAME . 'show',
                'title' => 'profile.show'
            ]
        ];
    }
}