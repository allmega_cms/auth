<?php

/**
 * This file is part of the Allmega Auth Bundle package.
 *
 * @copyright Allmega 
 * @package   Blog Bundle
 * @author    Eduard Jung <eddie@allmega.de>
 * @license   https://opensource.org/licenses/gpl-license.php GNU Public License
 */

namespace Allmega\AuthBundle\Manager;

use Allmega\AuthBundle\Data;
use Allmega\AuthBundle\Entity\User;
use Allmega\AuthBundle\Form\Model\{ChangePassword, NewPassword};
use Allmega\AuthBundle\Form\{ChangePasswordType, NewPasswordType};
use Symfony\Component\HttpFoundation\Response;

trait ChangePasswordTrait
{
    private string $changePassword = 'change_password';

    protected function changepw(): Response
    {
        $this->params->setAction($this->changePassword);
        $admin = $this->isGranted(Data::ADMIN_ROLE);
        
        $formType = $admin ? NewPasswordType::class : ChangePasswordType::class;
        $passwordForm = $admin ? new NewPassword() : new ChangePassword();

        $this->form = $this->createForm($formType, $passwordForm);
        $this->form->handleRequest($this->getRequest());

        if ($this->form->isSubmitted() && $this->form->isValid()) {
            $password = $this->getHashedPassword($passwordForm);
            $this->params
                ->setTransId('user.password_changed')
                ->getEntity()
                ->setPassword($password);

            return $this->doSave();
        }

        $user = $admin ? $this->params->getEntity() : new User();
        $this->params->addOptions([
            'action' => $this->params->getAction(),
            'form' => $this->form->createView(),
            'user' => $user,
        ]);
        return $this->getForm();
    }

    private function getHashedPassword(NewPassword|ChangePassword $passwordForm): string
    {
        return $this->services
            ->getService('passwordHasher')
            ->hashPassword($this->params->getEntity(), $passwordForm->getNewPassword());
    }
}