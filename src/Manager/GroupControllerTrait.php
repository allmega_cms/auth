<?php

/**
 * This file is part of the Allmega Auth Bundle package.
 *
 * @copyright Allmega 
 * @package   Blog Bundle
 * @author    Eduard Jung <eddie@allmega.de>
 * @license   https://opensource.org/licenses/gpl-license.php GNU Public License
 */

namespace Allmega\AuthBundle\Manager;

use Allmega\AuthBundle\Entity\Group;
use Allmega\AuthBundle\{Data, Events};
use Allmega\AuthBundle\Form\GroupType;
use Allmega\AuthBundle\Controller\GroupController;
use Allmega\BlogBundle\Utils\Params\BaseControllerParams;
use Symfony\Component\HttpFoundation\Response;

trait GroupControllerTrait
{
    protected function preExecution(): static
    {
        switch ($this->params->getAction()) {
            case $this->delete:
                $this->checkGroup();
                break;
            default:
        }
        return $this;
    }

    private function checkGroup(): void
    {
        $group = $this->params->getEntity();
        $adminGroupExists = false;
        $deleteGroup = false;

        // It must be checked that the last admin group is not deleted
        // The group must have ROLE_ADMIN and active users
        if (!$group->isSys() && $group->hasRole(Data::ADMIN_ROLE) && $group->getUsers()->count()) {
            $deleteGroup = true;
            $groups = $this->groupRepo->findNotEqualToGroupId($group->getId());
            foreach ($groups as $row) {
                if ($row->hasRole(Data::ADMIN_ROLE) && $row->getUsers()->count()) {
                    $adminGroupExists = true;
                }
            }
        }

        if ($group->isSys() || $deleteGroup && !$adminGroupExists) {
            $this->params->setEventName(Events::GROUP_DELETE_FAILED);
            $this->cancel();
        } else {
            $group->getGrouptypes()->clear();
            $group->getRoles()->clear();
            $group->getUsers()->clear();
        }
    }

    private function save(Group $group = null, array $arguments = []): Response
    {
        $eventName = $group ? Events::GROUP_UPDATED : Events::GROUP_CREATED;
        $group = $group ?? new Group();

        $params = (new BaseControllerParams())->init(
            arguments: $arguments,
            formParams: ['item' => $group],
            entity: $group,
            domain: Data::DOMAIN,
            eventName: $eventName,
            formType: GroupType::class,
            routeName: GroupController::ROUTE_NAME,
            templatesPath: GroupController::ROUTE_TEMPLATE_PATH
        );
        return $this->handle($params);
    }
}
