<?php

/**
 * This file is part of the Allmega Auth Bundle package.
 *
 * @copyright Allmega 
 * @package   Auth Bundle
 * @author    Eduard Jung <eddie@allmega.de>
 * @license   https://opensource.org/licenses/gpl-license.php GNU Public License
 */

namespace Allmega\AuthBundle\Controller;

use Allmega\AuthBundle\Entity\User;
use Allmega\AuthBundle\{Data, Events};
use Allmega\BlogBundle\Utils\Search\SearchItem;
use Allmega\AuthBundle\Repository\UserRepository;
use Allmega\AuthBundle\Manager\UserControllerTrait;
use Allmega\BlogBundle\Utils\{Paginator, SortableItem};
use Allmega\BlogBundle\Utils\Params\BaseControllerParams;
use Allmega\BlogBundle\Model\Controller\{BaseController, BaseControllerServices};
use Symfony\Component\Security\Http\Attribute\IsGranted;
use Symfony\Component\Routing\Attribute\Route;
use Symfony\Component\HttpFoundation\Response;

#[Route('/user', name: 'allmega_auth_user_')]
class UserController extends BaseController
{
    use UserControllerTrait;

    public const ROUTE_TEMPLATE_PATH = '@AllmegaAuth/user/';
    public const ROUTE_NAME = 'allmega_auth_user_';
    public const PROP = 'user';

    public function __construct(
        private readonly UserRepository $userRepo,
        private readonly string $usersEnabled,
        private readonly int $usersDelete,
        BaseControllerServices $services)
    {
        parent::__construct($services);
    }

    #[Route('/search', name: 'search', methods: 'GET')]
    #[IsGranted('auth-user-search')]
    public function search(SearchItem $item): Response
    {
        $item
            ->setTemplatePath(self::ROUTE_TEMPLATE_PATH)
            ->setRoute(self::ROUTE_NAME . 'show')
            ->setProps(['fullname'])
            ->getRepo()
            ->setSearch($this->userRepo)
            ->addOptions(['user' => $this->getUser()]);

        $item->getTrans()->setLabel('user.search.title')->setDomain(Data::DOMAIN);       
        return $this->json(['data' => $item->getSearchResult()]);
    }

    #[Route('/list', name: 'index', methods: 'GET')]
    #[IsGranted('auth-user-list')]
    public function index(Paginator $paginator): Response
    {
        $query = $this->userRepo->findAllQuery();
        return $this->render(self::ROUTE_TEMPLATE_PATH . 'index.html.twig', [
            'params' => $this->getTemplateParams($this, Data::DOMAIN),
            'item' => SortableItem::getInstance(new User()),
            'users' => $paginator->getPagination($query)
        ]);
    }

    #[Route('/show/{id}', name: 'show', methods: 'GET')]
    #[IsGranted('auth-user-show', subject: self::PROP)]
    public function show(User $user): Response
    {
        $params = ['params' => $this->getTemplateParams($this, Data::DOMAIN), 'user' => $user];
        if ($this->isXmlRequest) {
            $data = [
                'content' =>  $this->renderView(self::ROUTE_TEMPLATE_PATH . 'inc/_show.html.twig', $params)
            ];
            return $this->json($data);
        }
        return $this->render(self::ROUTE_TEMPLATE_PATH . 'show.html.twig', $params);
    }

    #[Route('/add', name: 'add', methods: ['GET', 'POST'])]
    #[IsGranted('auth-user-add')]
    public function add(): Response
    {
        return $this->save();
    }

    #[Route('/edit/{id}', name: 'edit', methods: ['GET', 'POST'])]
    #[IsGranted('auth-user-edit', subject: self::PROP)]
    public function edit(User $user): Response
    {
        return $this->save($user);
    }

    #[Route('/state/{id}', name: 'state', methods: 'GET')]
    #[IsGranted('auth-user-state', subject: self::PROP)]
    public function changeState(User $user): Response
    {
        $params = (new BaseControllerParams())->init(
            entity: $user,
            domain: Data::DOMAIN,
            eventName: Events::USER_STATE_CHANGED,
            method: 'enabled',
            routeName: self::ROUTE_NAME
        );
        return $this->handle($params, $this->state);
    }

    #[Route('/verify/{id}', name: 'verify', methods: 'GET')]
    #[IsGranted('auth-user-verify', subject: self::PROP)]
    public function changeVerify(User $user): Response
    {
        $params = (new BaseControllerParams())->init(
            entity: $user,
            domain: Data::DOMAIN,
            eventName: Events::USER_VERIFIED_CHANGED,
            method: 'verified',
            routeName: self::ROUTE_NAME
        );
        return $this->handle($params, $this->state);
    }

    #[Route('/{id}', name: 'delete', methods: 'DELETE')]
    #[IsGranted('auth-user-delete', subject: self::PROP)]
    public function delete(User $user): Response
    {
        if ($this->usersDelete != 1) {
            return $this->redirectToRoute(self::ROUTE_NAME. $this->index, [], Response::HTTP_SEE_OTHER);
        }

        $params = (new BaseControllerParams())->init(
            entity: $user,
            domain: Data::DOMAIN,
            eventName: Events::USER_DELETED,
            routeName: self::ROUTE_NAME
        );
        return $this->handle($params, $this->delete);
    }

    #[Route('/change-password/{id}', name: 'change_password', methods: ['GET', 'POST'])]
    #[IsGranted('auth-user-changepassword', subject: self::PROP)]
    public function changePassword(User $user): Response
    {
        return $this->changeUserPassword($user);
    }

    #[Route('/dashboard', name: 'dashboard', methods: 'GET')]
    public function getDashboardWidget(): Response
    {
        if (!$this->isGranted('auth-user-dashboard')) return new Response();
        
        $users = $this->userRepo->findByLastLogged();
        return $this->render(self::ROUTE_TEMPLATE_PATH . 'dashboard.html.twig', [
            'params' => $this->getTemplateParams($this, Data::DOMAIN),
            'users' => $users,
        ]);
    }
}