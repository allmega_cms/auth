<?php

/**
 * This file is part of the Allmega Auth Bundle package.
 *
 * @copyright Allmega 
 * @package   Auth Bundle
 * @author    Eduard Jung <eddie@allmega.de>
 * @license   https://opensource.org/licenses/gpl-license.php GNU Public License
 */

namespace Allmega\AuthBundle\Controller;

use Allmega\AuthBundle\Data;
use Allmega\BlogBundle\Model\Controller\{BaseController, BaseControllerServices};
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;
use Symfony\Component\Routing\Attribute\Route;
use Symfony\Component\HttpFoundation\Response;

#[Route(name: 'allmega_auth_')]
class SecurityController extends BaseController
{
    public const ROUTE_TEMPLATE_PATH = '@AllmegaAuth/security/';
    public const ROUTE_NAME = 'allmega_auth_';

    public function __construct(BaseControllerServices $services)
    {
        parent::__construct($services);
    }

    #[Route('/login', name: 'login')]
    public function login(AuthenticationUtils $authenticationUtils): Response
    {
        $redirect = [];
        $query = $this->getRequest()->query->get('redirect_to');
        if ($query) $redirect['redirect_to'] = $query;

        $error = $authenticationUtils->getLastAuthenticationError();
        $lastUsername = $authenticationUtils->getLastUsername();

        $optParams = ['registrationRoute' => RegistrationController::ROUTE_NAME];
        $options = [
            'params' => $this->getTemplateParams($this, Data::DOMAIN, $optParams),
            'last_username' => $lastUsername,
            'redirect' => $redirect,
            'error' => $error,
            'action' => 'login'
        ];

        if (!$this->isXmlRequest) return $this->render(self::ROUTE_TEMPLATE_PATH . 'form.html.twig', $options);

        $data['content'] = $this->renderView(self::ROUTE_TEMPLATE_PATH . 'forms/_login.html.twig', $options);
        return $this->json($data);
    }

    #[Route('/logout', name: 'logout', methods: 'GET')]
    public function logout() {}

    #[Route('/icon', name: 'icon')]
    public function getMenuIcon(string $action = 'login'): Response
    {
        return $this->render(self::ROUTE_TEMPLATE_PATH . 'links/_'.$action.'.html.twig', [
            'params' => $this->getTemplateParams($this, Data::DOMAIN)
        ]);
    }
}