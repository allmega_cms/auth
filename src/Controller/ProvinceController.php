<?php

/**
 * This file is part of the Allmega Auth Bundle package.
 *
 * @copyright Allmega 
 * @package   Auth Bundle
 * @author    Eduard Jung <eddie@allmega.de>
 * @license   https://opensource.org/licenses/gpl-license.php GNU Public License
 */

namespace Allmega\AuthBundle\Controller;

use Allmega\AuthBundle\{Data, Events};
use Allmega\AuthBundle\Entity\Province;
use Allmega\AuthBundle\Repository\ProvinceRepository;
use Allmega\AuthBundle\Manager\ProvinceControllerTrait;
use Allmega\BlogBundle\Utils\Params\BaseControllerParams;
use Allmega\BlogBundle\Model\Controller\{BaseController, BaseControllerServices};
use Symfony\Component\Security\Http\Attribute\IsGranted;
use Symfony\Component\Routing\Attribute\Route;
use Symfony\Component\HttpFoundation\Response;

#[Route('/province', name: 'allmega_auth_province_')]
class ProvinceController extends BaseController
{
    use ProvinceControllerTrait;

    public const ROUTE_TEMPLATE_PATH = '@AllmegaAuth/province/';
    public const ROUTE_NAME = 'allmega_auth_province_';
    public const PROP = 'province';

    public function __construct(BaseControllerServices $services)
    {
        parent::__construct($services);
    }

    #[Route('/list', name: 'index', methods: 'GET')]
    #[IsGranted('auth-province-list')]
    public function index(ProvinceRepository $provinceRepo): Response
    {
        return $this->render(self::ROUTE_TEMPLATE_PATH . 'index.html.twig', [
            'params' => $this->getTemplateParams($this, Data::DOMAIN),
            'provinces' => $provinceRepo->findBy([], ['name' => 'ASC'])
        ]);
    }

    #[Route('/add', name: 'add', methods: ['GET', 'POST'])]
    #[IsGranted('auth-province-add')]
    public function add(): Response
    {
        return $this->save();
    }

    #[Route('/edit/{id}', name: 'edit', methods: ['GET', 'POST'])]
    #[IsGranted('auth-province-edit', subject: self::PROP)]
    public function edit(Province $province): Response
    {
        return $this->save($province);
    }

    #[Route('/state/{id}', name: 'state', methods: 'GET')]
    #[IsGranted('auth-province-state', subject: self::PROP)]
    public function changeState(Province $province): Response
    {
        $params = (new BaseControllerParams())->init(
            entity: $province,
            domain: Data::DOMAIN,
            eventName: Events::PROVINCE_STATE_CHANGED,
            routeName: self::ROUTE_NAME
        );
        return $this->handle($params, $this->state);
    }

    #[Route('/{id}', name: 'delete', methods: 'DELETE')]
    #[IsGranted('auth-province-delete', subject: self::PROP)]
    public function delete(Province $province): Response
    {
        $params = (new BaseControllerParams())->init(
            entity: $province,
            domain: Data::DOMAIN,
            eventName: Events::PROVINCE_DELETED,
            routeName: self::ROUTE_NAME
        );
        return $this->handle($params, $this->delete);
    }
}