<?php

/**
 * This file is part of the Allmega Auth Bundle package.
 *
 * @copyright Allmega 
 * @package   Auth Bundle
 * @author    Eduard Jung <eddie@allmega.de>
 * @license   https://opensource.org/licenses/gpl-license.php GNU Public License
 */

namespace Allmega\AuthBundle\Controller;

use Allmega\AuthBundle\Entity\Group;
use Allmega\AuthBundle\{Data, Events};
use Allmega\BlogBundle\Utils\Search\SearchItem;
use Allmega\AuthBundle\Repository\GroupRepository;
use Allmega\AuthBundle\Manager\GroupControllerTrait;
use Allmega\BlogBundle\Utils\{Paginator, SortableItem};
use Allmega\BlogBundle\Utils\Params\BaseControllerParams;
use Allmega\BlogBundle\Model\Controller\{BaseController, BaseControllerServices};
use Symfony\Component\Security\Http\Attribute\IsGranted;
use Symfony\Component\Routing\Attribute\Route;
use Symfony\Component\HttpFoundation\Response;

#[Route('/group', name: 'allmega_auth_group_')]
class GroupController extends BaseController
{
    use GroupControllerTrait;

    public const ROUTE_TEMPLATE_PATH = '@AllmegaAuth/group/';
    public const ROUTE_NAME = 'allmega_auth_group_';
    public const PROP = 'group';

    public function __construct(
        private readonly GroupRepository $groupRepo,
        BaseControllerServices $services)
    {
        parent::__construct($services);
    }

    #[Route('/search', name: 'search', methods: 'GET')]
    #[IsGranted('auth-group-search')]
    public function search(SearchItem $item): Response
    {
        $item
            ->setTemplatePath(self::ROUTE_TEMPLATE_PATH)
            ->setRoute(self::ROUTE_NAME . 'show')
            ->setProps(['name'])
            ->getRepo()
            ->setSearch($this->groupRepo)
            ->addOptions(['user' => $this->getUser()]);

        $item->getTrans()->setLabel('group.search.title')->setDomain(Data::DOMAIN);       
        return $this->json(['data' => $item->getSearchResult()]);
    }

    #[Route('/list', name: 'index', methods: 'GET')]
    #[IsGranted('auth-group-list')]
    public function index(Paginator $paginator): Response
    {
        $query = $this->groupRepo->findAllQuery();
        return $this->render(self::ROUTE_TEMPLATE_PATH . 'index.html.twig', [
            'params' => $this->getTemplateParams($this, Data::DOMAIN),
            'item' => SortableItem::getInstance(new Group()),
            'groups' => $paginator->getPagination($query)
        ]);
    }

    #[Route('/show/{id}', name: 'show', methods: 'GET')]
    #[IsGranted('auth-group-show', subject: self::PROP)]
    public function show(Group $group): Response
    {
        $params = ['params' => $this->getTemplateParams($this, Data::DOMAIN), 'group' => $group];
        if ($this->isXmlRequest) {
            $data = [
                'content' =>  $this->renderView(self::ROUTE_TEMPLATE_PATH . 'show/_layout.html.twig', $params)
            ];
            return $this->json($data);
        }
        return $this->render(self::ROUTE_TEMPLATE_PATH . 'show.html.twig', $params);
    }

    #[Route('/add', name: 'add', methods: ['GET', 'POST'])]
    #[IsGranted('auth-group-add')]
    public function add(): Response
    {
        return $this->save();
    }

    #[Route('/edit/{id}', name: 'edit', methods: ['GET', 'POST'])]
    #[IsGranted('auth-group-edit', subject: self::PROP)]
    public function edit(Group $group): Response
    {
        return $this->save($group);
    }

    #[Route('/{id}', name: 'delete', methods: 'DELETE')]
    #[IsGranted('auth-group-delete', subject: self::PROP)]
    public function delete(Group $group): Response
    {
        $params = (new BaseControllerParams())->init(
            entity: $group,
            domain: Data::DOMAIN,
            eventName: Events::GROUP_DELETED,
            routeName: self::ROUTE_NAME
        );
        return $this->handle($params, $this->delete);
    }
}