<?php

/**
 * This file is part of the Allmega Auth Bundle package.
 *
 * @copyright Allmega
 * @package   Auth Bundle
 * @author    Eduard Jung <eddie@allmega.de>
 * @license   https://opensource.org/licenses/gpl-license.php GNU Public License
 */

namespace Allmega\AuthBundle\Controller;

use Allmega\AuthBundle\{Data, Events};
use Allmega\AuthBundle\Security\EmailVerifier;
use Allmega\BlogBundle\Controller\DashboardController;
use Allmega\AuthBundle\Manager\RegistrationControllerTrait;
use Allmega\BlogBundle\Model\Controller\{BaseController, BaseControllerServices};
use Symfony\Component\Security\Http\Attribute\IsGranted;
use SymfonyCasts\Bundle\VerifyEmail\Exception\VerifyEmailExceptionInterface;
use Symfony\Component\EventDispatcher\GenericEvent;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Attribute\Route;

#[Route('/registration', name: 'allmega_auth_registration_')]
class RegistrationController extends BaseController
{
    use RegistrationControllerTrait;

    public const ROUTE_TEMPLATE_PATH = '@AllmegaAuth/registration/';
    public const ROUTE_NAME = 'allmega_auth_registration_';

    public function __construct(
        private readonly EmailVerifier $emailVerifier,
        BaseControllerServices $services)
    {
        parent::__construct($services);
    }

    #[Route('/register', name: 'add')]
    #[IsGranted('auth-registration-add')]
    public function add(): Response
    {
        return $this->save();
    }

    #[Route('/verify/email', name: 'verify_email')]
    #[IsGranted('auth-registration-verify')]
    public function verifyUserEmail(): Response
    {
        $this->denyAccessUnlessGranted('IS_AUTHENTICATED_FULLY');
        try {
            $this->emailVerifier->handleEmailConfirmation($this->getRequest(), $this->getUser());
        } catch (VerifyEmailExceptionInterface $exception) {
            $message = $this->translate($exception->getReason(), [], 'VerifyEmailBundle');

            $this->addFlash('danger', $message);
            return $this->redirectToRoute(self::ROUTE_NAME . $this->add);
        }

        $event = new GenericEvent($this->getUser(), [
            'isXmlRequest' => $this->isXmlRequest,
            'domain' => Data::DOMAIN
        ]);

        $this->services
            ->getService('eventDispatcher')
            ->dispatch($event, Events::REGISTRATION_COMPLETED);

        return $this->redirectToRoute(DashboardController::ROUTE_NAME . $this->show);
    }
}