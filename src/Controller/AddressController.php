<?php

/**
 * This file is part of the Allmega Auth Bundle package.
 *
 * @copyright Allmega 
 * @package   Auth Bundle
 * @author    Eduard Jung <eddie@allmega.de>
 * @license   https://opensource.org/licenses/gpl-license.php GNU Public License
 */

namespace Allmega\AuthBundle\Controller;

use Allmega\AuthBundle\{Data, Events};
use Allmega\AuthBundle\Entity\Address;
use Allmega\AuthBundle\Manager\AddressControllerTrait;
use Allmega\BlogBundle\Utils\Params\BaseControllerParams;
use Allmega\BlogBundle\Model\Controller\{BaseController, BaseControllerServices};
use Symfony\Component\Security\Http\Attribute\IsGranted;
use Symfony\Component\Routing\Attribute\Route;
use Symfony\Component\HttpFoundation\Response;

#[Route('/address', name: 'allmega_auth_address_')]
class AddressController extends BaseController
{
    use AddressControllerTrait;
    
    public const ROUTE_TEMPLATE_PATH = '@AllmegaAuth/address/';
    public const ROUTE_NAME = 'allmega_auth_address_';
    public const PROP = 'address';

    public function __construct(BaseControllerServices $services)
    {
        parent::__construct($services);
    }

    #[Route('/add', name: 'add', methods: ['GET', 'POST'])]
    #[IsGranted('auth-address-add')]
    public function add(): Response
    {
        return $this->save();
    }

    #[Route('/edit/{id}', name: 'edit', methods: ['GET', 'POST'])]
    #[IsGranted('auth-address-edit', subject: self::PROP)]
    public function edit(Address $address): Response
    {
        return $this->save($address);
    }

    #[Route('/{id}', name: 'delete', methods: 'DELETE')]
    #[IsGranted('auth-address-delete', subject: self::PROP)]
    public function delete(Address $address): Response
    {
        $params = (new BaseControllerParams())->init(
            entity: $address,
            domain: Data::DOMAIN,
            eventName: Events::ADDRESS_DELETED
        );
        return $this->handle($params, $this->delete);
    }
}