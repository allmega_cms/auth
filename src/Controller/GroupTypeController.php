<?php

/**
 * This file is part of the Allmega Auth Bundle package.
 *
 * @copyright /srv/vhosts/cms.home.lan/vendor/allmega/authAllmega 
 * @package   Auth Bundle
 * @author    Eduard Jung <eddie@allmega.de>
 * @license   https://opensource.org/licenses/gpl-license.php GNU Public License
 */

namespace Allmega\AuthBundle\Controller;

use Allmega\AuthBundle\{Data, Events};
use Allmega\AuthBundle\Entity\GroupType;
use Allmega\AuthBundle\Repository\GroupTypeRepository;
use Allmega\AuthBundle\Manager\GrouptypeControllerTrait;
use Allmega\BlogBundle\Model\Controller\{BaseController, BaseControllerServices};
use Allmega\BlogBundle\Utils\Params\BaseControllerParams;
use Symfony\Component\Security\Http\Attribute\IsGranted;
use Symfony\Component\Routing\Attribute\Route;
use Symfony\Component\HttpFoundation\Response;

#[Route('/grouptype', name: 'allmega_auth_grouptype_')]
class GroupTypeController extends BaseController
{
    use GrouptypeControllerTrait;

    public const ROUTE_TEMPLATE_PATH = '@AllmegaAuth/grouptype/';
    public const ROUTE_NAME = 'allmega_auth_grouptype_';
    public const PROP = 'grouptype';

    public function __construct(BaseControllerServices $services)
    {
        parent::__construct($services);
    }

    #[Route('/list', name: 'index', methods: 'GET')]
    #[IsGranted('auth-grouptype-list')]
    public function index(GroupTypeRepository $groupTypeRepo): Response
    {
        return $this->render(self::ROUTE_TEMPLATE_PATH . 'index.html.twig', [
            'grouptypes' => $groupTypeRepo->findBy([], ['name' => 'ASC']),
            'params' => $this->getTemplateParams($this, Data::DOMAIN, [
                'groupPath' => GroupController::ROUTE_TEMPLATE_PATH,
                'groupRoute' => GroupController::ROUTE_NAME,
            ])
        ]);
    }

    #[Route('/show/{id}', name: 'show', methods: 'GET')]
    #[IsGranted('auth-grouptype-show', subject: self::PROP)]
    public function show(GroupType $grouptype): Response
    {
        $params = ['params' => $this->getTemplateParams($this, Data::DOMAIN), 'grouptype' => $grouptype];
        if ($this->isXmlRequest) {
            $data = [
                'content' =>  $this->renderView(self::ROUTE_TEMPLATE_PATH . 'show/_layout.html.twig', $params)
            ];
            return $this->json($data);
        }
        return $this->render(self::ROUTE_TEMPLATE_PATH . 'show.html.twig', $params);
    }

    #[Route('/add', name: 'add', methods: ['GET', 'POST'])]
    #[IsGranted('auth-grouptype-add')]
    public function add(): Response
    {
        return $this->save();
    }

    #[Route('/edit/{id}', name: 'edit', methods: ['GET', 'POST'])]
    #[IsGranted('auth-grouptype-edit', subject: self::PROP)]
    public function edit(GroupType $grouptype): Response
    {
        return $this->save($grouptype);
    }

    #[Route('/{id}', name: 'delete', methods: 'DELETE')]
    #[IsGranted('auth-grouptype-delete', subject: self::PROP)]
    public function delete(GroupType $grouptype): Response
    {
        $params = (new BaseControllerParams())->init(
            entity: $grouptype,
            domain: Data::DOMAIN,
            eventName: Events::GROUPTYPE_DELETED,
            routeName: self::ROUTE_NAME
        );
        return $this->handle($params, $this->delete);
    }
}