<?php

/**
 * This file is part of the Allmega Auth Bundle package.
 *
 * @copyright Allmega 
 * @package   Auth Bundle
 * @author    Eduard Jung <eddie@allmega.de>
 * @license   https://opensource.org/licenses/gpl-license.php GNU Public License
 */

namespace Allmega\AuthBundle\Controller;

use Allmega\AuthBundle\Data;
use Allmega\AuthBundle\Entity\User;
use Allmega\BlogBundle\Entity\Notification;
use Allmega\AuthBundle\Form\Model\NewPassword;
use Allmega\BlogBundle\Controller\DashboardController;
use Allmega\AuthBundle\Form\{NewPasswordType, ResetPasswordType};
use Allmega\BlogBundle\Model\Controller\{BaseController, BaseControllerServices};
use SymfonyCasts\Bundle\ResetPassword\Exception\ResetPasswordExceptionInterface;
use SymfonyCasts\Bundle\ResetPassword\Controller\ResetPasswordControllerTrait;
use SymfonyCasts\Bundle\ResetPassword\ResetPasswordHelperInterface;
use Symfony\Component\HttpFoundation\{RedirectResponse, Response};
use Symfony\Component\Routing\Attribute\Route;

#[Route('/reset-password', name: 'allmega_auth_password_')]
class ResetPasswordController extends BaseController
{
    use ResetPasswordControllerTrait;

    public const ROUTE_TEMPLATE_PATH = '@AllmegaAuth/reset_password/';
    public const ROUTE_NAME = 'allmega_auth_password_';

    public function __construct(
        private readonly ResetPasswordHelperInterface $resetPasswordHelper,
        BaseControllerServices $services)
    {
        parent::__construct($services);
    }

    #[Route('', name: 'forgot')]
    public function forgot(): Response
    {
        $form = $this->createForm(ResetPasswordType::class);
        $form->handleRequest($this->getRequest());

        if ($form->isSubmitted() && $form->isValid()) {
            return $this->processSendingPasswordResetEmail($form->get('email')->getData());
        }

        $title = 'reset_password.forgot.link';
        $optParams = [
            'loginPath' => SecurityController::ROUTE_NAME . 'login',
            'routePath' => self::ROUTE_NAME . 'forgot',
            'label' => $title, 'title' => $title,
            'form' => $form->createView(),
        ];

        $params = $this->getTemplateParams($this, Data::DOMAIN, $optParams);
        if ($this->isXmlRequest) {
            $data = [
                'content' =>  $this->renderView(
                    self::ROUTE_TEMPLATE_PATH . 'inc/_form.html.twig',
                    ['params' => $params])
            ];
            return $this->json($data);
        }

        return $this->render(self::ROUTE_TEMPLATE_PATH . 'form.html.twig', [
            'params' => $params,
        ]);
    }

    /**
     * Confirmation page after a user has requested a password reset.
     */
    #[Route('/check-email', name: 'check_email')]
    public function checkEmail(): Response
    {
        // Generate a fake token if the user does not exist or someone hit this page directly.
        // This prevents exposing whether or not a user was found with the given email address or not
        if (null === ($resetToken = $this->getTokenObjectFromSession())) {
            $resetToken = $this->resetPasswordHelper->generateFakeResetToken();
        }

        $params = $this->getTemplateParams($this, Data::DOMAIN, ['check' => true]);
        return $this->render(self::ROUTE_TEMPLATE_PATH . 'check_email.html.twig', [
            'resetToken' => $resetToken, 'params' => $params,
        ]);
    }

    /**
     * Validates and process the reset URL that the user clicked in their email.
     */
    #[Route('/reset/{token}', name: 'reset')]
    public function reset(string $token = null): Response
    {
        if ($token) {
            // We store the token in session and remove it from the URL, to avoid the URL being
            // loaded in a browser and potentially leaking the token to 3rd party JavaScript.
            $this->storeTokenInSession($token);
            return $this->redirectToRoute(self::ROUTE_NAME . 'reset');
        }

        $token = $this->getTokenFromSession();
        if (null === $token) {
            throw $this->createNotFoundException('No reset password token found in the URL or in the session.');
        }

        try {
            $user = $this->resetPasswordHelper->validateTokenAndFetchUser($token);
        } catch (ResetPasswordExceptionInterface $e) {
            $this->addFlash('reset_password_error', sprintf(
                '%s - %s',
                $this->translate(ResetPasswordExceptionInterface::MESSAGE_PROBLEM_VALIDATE, [], 'ResetPasswordBundle'),
                $this->translate($e->getReason(), [], 'ResetPasswordBundle')
            ));

            return $this->redirectToRoute(self::ROUTE_NAME . 'forgot');
        }

        // The token is valid; allow the user to change their password.
        $newPassword = new NewPassword();
        $form = $this->createForm(NewPasswordType::class, $newPassword);
        $form->handleRequest($this->getRequest());

        if ($form->isSubmitted() && $form->isValid()) {
            // A password reset token should be used only once, remove it.
            $this->resetPasswordHelper->removeResetRequest($token);

            // Encode(hash) the plain password, and set it.
            $passwordHasher = $this->services->getService('passwordHasher');
            $encodedPassword = $passwordHasher->hashPassword($user, $newPassword->getNewPassword());
            $user->setPassword($encodedPassword);
            $this->getEntityManager()->flush();

            // The session is cleaned up after the password has been changed.
            $this->cleanSessionAfterReset();
            return $this->redirectToRoute(DashboardController::ROUTE_NAME . 'show');
        }

        $title = 'reset_password.forgot.link';
        $optParams = [
            'routePath' => self::ROUTE_NAME . 'reset',
            'label' => '', 'title' => $title,
            'form' => $form->createView(),
        ];

        return $this->render(self::ROUTE_TEMPLATE_PATH . 'form.html.twig', [
            'params' => $this->getTemplateParams($this, Data::DOMAIN, $optParams),
        ]);
    }

    private function processSendingPasswordResetEmail(string $emailFormData): RedirectResponse
    {
        $user = $this->getEntityManager()->getRepository(User::class)->findOneBy(['email' => $emailFormData]);

        // Do not reveal whether a user account was found or not.
        if (!$user) return $this->redirectToRoute(self::ROUTE_NAME . 'check_email');

        try {
            $resetToken = $this->resetPasswordHelper->generateResetToken($user);
        } catch (ResetPasswordExceptionInterface $e) {
            // If you want to tell the user why a reset email was not sent, uncomment
            // the lines below and change the redirect to 'app_forgot_password_request'.
            // Caution: This may reveal if a user is registered or not.
            //
            // $this->addFlash('reset_password_error', sprintf(
            //     '%s - %s',
            //     $this->translata(ResetPasswordExceptionInterface::MESSAGE_PROBLEM_HANDLE, [], 'ResetPasswordBundle'),
            //     $this->translata($e->getReason(), [], 'ResetPasswordBundle')
            // ));

            return $this->redirectToRoute(self::ROUTE_NAME . 'check_email');
        }

        $subject = $this->translate('reset_password.forgot.link', [], Data::DOMAIN);
        $notification = (new Notification())
            ->setTemplate(self::ROUTE_TEMPLATE_PATH . 'email.html.twig')
            ->setParams(['resetToken' => $resetToken])
            ->setReceivers([$user->getEmail()])
            ->setSubject($subject);

        $this->services->getService('bus')->dispatch($notification);

        // Store the token object in session for retrieval in check-email route.
        $this->setTokenObjectInSession($resetToken);
        return $this->redirectToRoute(self::ROUTE_NAME . 'check_email');
    }
}