<?php

/**
 * This file is part of the Allmega Auth Bundle package.
 *
 * @copyright Allmega
 * @package   Auth Bundle
 * @author    Eduard Jung <eddie@allmega.de>
 * @license   https://opensource.org/licenses/gpl-license.php GNU Public License
 */

namespace Allmega\AuthBundle\Controller;

use Allmega\AuthBundle\Entity\User;
use Allmega\BlogBundle\Model\Controller\{BaseController, BaseControllerServices};
use Symfony\Component\Security\Http\Attribute\CurrentUser;
use Symfony\Component\HttpFoundation\Response;

class ApiSecurityController extends BaseController
{
    public function __construct(BaseControllerServices $services)
    {
        parent::__construct($services);
    }

    public function login(#[CurrentUser] ?User $user): Response
    {
        return $this->json([]);
    }
}