<?php

/**
 * This file is part of the Allmega Auth Bundle package.
 *
 * @copyright Allmega 
 * @package   Auth Bundle
 * @author    Eduard Jung <eddie@allmega.de>
 * @license   https://opensource.org/licenses/gpl-license.php GNU Public License
 */

namespace Allmega\AuthBundle\Controller;

use Allmega\AuthBundle\Data;
use Allmega\AuthBundle\Manager\ProfileControllerTrait;
use Allmega\BlogBundle\Model\Controller\{BaseController, BaseControllerServices};
use Symfony\Component\Security\Http\Attribute\IsGranted;
use Symfony\Component\Routing\Attribute\Route;
use Symfony\Component\HttpFoundation\Response;

#[Route('/profile', name: 'allmega_auth_profile_')]
class ProfileController extends BaseController
{
    use ProfileControllerTrait;

    public const ROUTE_TEMPLATE_PATH = '@AllmegaAuth/profile/';
    public const ROUTE_NAME = 'allmega_auth_profile_';

    public function __construct(BaseControllerServices $services)
    {
        parent::__construct($services);
    }

    #[Route('/show', name: 'show', methods: 'GET')]
    #[IsGranted('auth-profile-show')]
    public function show(): Response
    {
        $optParams = [
            'address' => [
                'path' => AddressController::ROUTE_TEMPLATE_PATH,
                'route' => AddressController::ROUTE_NAME,
            ],
            'user' => ['path' => UserController::ROUTE_TEMPLATE_PATH],
        ];
        
        return $this->render(self::ROUTE_TEMPLATE_PATH . 'show.html.twig', [
            'params' => $this->getTemplateParams($this, Data::DOMAIN, $optParams),
            'user' => $this->getUser()
        ]);
    }

    #[Route('/edit', name: 'edit', methods: ['GET', 'POST'])]
    #[IsGranted('auth-profile-edit')]
    public function edit(): Response
    {
        return $this->save();
    }

    #[Route('/change-password', name: 'change_password', methods: ['GET', 'POST'])]
    #[IsGranted('auth-profile-changepassword')]
    public function changePassword(): Response
    {
        return $this->changeProfilePassword();
    }

    #[Route('/dashboard', name: 'dashboard', methods: 'GET')]
    public function getDashboardWidget(): Response
    {
        if (!$this->isGranted('auth-profile-dashboard')) return new Response();
        
        $optParams = ['user' => ['path' => UserController::ROUTE_TEMPLATE_PATH]];
        return $this->render(self::ROUTE_TEMPLATE_PATH . 'dashboard.html.twig', [
            'params' => $this->getTemplateParams($this, Data::DOMAIN, $optParams),
            'user' => $this->getUser()
        ]);
    }

    #[Route('/icon', name: 'icon', methods: 'GET')]
    public function getMenuIcon(): Response
    {
        if (!$this->isGranted('auth-profile-icon')) return new Response();

        return $this->render(self::ROUTE_TEMPLATE_PATH . 'inc/_icon.html.twig', [
            'params' => $this->getTemplateParams($this, Data::DOMAIN)
        ]);
    }
}