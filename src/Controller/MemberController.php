<?php

/**
 * This file is part of the Allmega Auth Bundle package.
 *
 * @copyright Allmega 
 * @package   Auth Bundle
 * @author    Eduard Jung <eddie@allmega.de>
 * @license   https://opensource.org/licenses/gpl-license.php GNU Public License
 */

namespace Allmega\AuthBundle\Controller;

use Allmega\AuthBundle\Data;
use Allmega\AuthBundle\Repository\{GroupRepository, UserRepository};
use Allmega\BlogBundle\Model\Controller\{BaseController, BaseControllerServices};
use Knp\Bundle\SnappyBundle\Snappy\Response\PdfResponse;
use Nucleos\DompdfBundle\Wrapper\DompdfWrapperInterface;
use Symfony\Component\Security\Http\Attribute\IsGranted;
use Symfony\Component\Routing\Attribute\Route;
use Symfony\Component\HttpFoundation\Response;

#[Route('/member', name: 'allmega_auth_member_')]
class MemberController extends BaseController
{
    public const ROUTE_TEMPLATE_PATH = '@AllmegaAuth/member/';
    public const ROUTE_NAME = 'allmega_auth_member_';

    public function __construct(
        private readonly UserRepository $userRepo,
        BaseControllerServices          $services)
    {
        parent::__construct($services);
    }

    #[Route('/list', name: 'index', methods: 'GET')]
    #[IsGranted('auth-member-list')]
    public function index(GroupRepository $groupRepo): Response
    {
        $groups = $groupRepo->findDepartments();
        return $this->render(self::ROUTE_TEMPLATE_PATH . 'index.html.twig', [
            'params' => $this->getTemplateParams($this, Data::DOMAIN),
            'groups' => $groups
        ]);
    }

    #[Route('/print', name: 'print', methods: 'GET')]
    #[IsGranted('auth-member-print')]
    public function renderPdf(DompdfWrapperInterface $wrapper): Response
    {
        $users = $this->userRepo->findBy(['enabled' => 1], ['lastname' => 'ASC']);
        $html  = $this->renderView(self::ROUTE_TEMPLATE_PATH . 'pdf.html.twig', ['users' => $users]);
        return new PdfResponse($wrapper->getPdf($html), 'phones.pdf');
    }

    #[Route('/online', name: 'online', methods: 'GET')]
    public function getSidePopupWidget(): Response
    {
        if (!$this->isGranted('auth-member-online')) return new Response();
        
        $users = $this->userRepo->findOnline();
        return $this->render(self::ROUTE_TEMPLATE_PATH . 'online.html.twig', [
            'params' => $this->getTemplateParams($this, Data::DOMAIN),
            'users' => $users
        ]);
    }
}