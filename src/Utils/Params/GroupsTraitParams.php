<?php

/**
 * This file is part of the Allmega Auth Bundle package.
 *
 * @copyright Allmega
 * @package   Auth Bundle
 * @author    Eduard Jung <eddie@allmega.de>
 * @license   https://opensource.org/licenses/gpl-license.php GNU Public License
 */

namespace Allmega\AuthBundle\Utils\Params;

use Allmega\BlogBundle\Utils\Params\BaseFormFieldsParams;

class GroupsTraitParams extends BaseFormFieldsParams
{
    private array $shortnames = [];

    public function getShortnames(): array
    {
        return $this->shortnames;
    }

    public function setShortnames(array $shortnames): static
    {
        $this->shortnames = $shortnames;
        return $this;
    }
}