<?php

/**
 * This file is part of the Allmega Auth Bundle package.
 *
 * @copyright Allmega 
 * @package   Auth Bundle
 * @author    Eduard Jung <eddie@allmega.de>
 * @license   https://opensource.org/licenses/gpl-license.php GNU Public License
 */

namespace Allmega\AuthBundle\Utils\Loader\Loaders;

use Allmega\BlogBundle\Utils\Loader\Model\AbstractLoader;
use Allmega\AuthBundle\Entity\GroupType;

class GroupTypesLoader extends AbstractLoader
{
    public function init(): static
    {
        $this->transId = 'loader.grouptypes.message';
        $this->title = 'loader.grouptypes.phrase';
        $this->class = GroupType::class;
        return $this;
    }

    public function prototype(object &$item, ?object $existsItem): void
    {
        if ($existsItem) {
            $item = $existsItem->setSelectable($item->isSelectable())->setSys($item->isSys());
        }
        $description = $this->trans("storage.grouptype.{$item->getShortname()}.description", $this->currentPackage);
        $name = $this->trans("storage.grouptype.{$item->getShortname()}.name", $this->currentPackage);
        $item->setName($name)->setDescription($description);
    }
}