<?php

/**
 * This file is part of the Allmega Auth Bundle package.
 *
 * @copyright Allmega 
 * @package   Auth Bundle
 * @author    Eduard Jung <eddie@allmega.de>
 * @license   https://opensource.org/licenses/gpl-license.php GNU Public License
 */

namespace Allmega\AuthBundle\Utils\Loader\Loaders;

use Allmega\AuthBundle\Entity\{Country, Group, Province, Role, User};
use Allmega\BlogBundle\Utils\Loader\Model\AbstractLoader;

class UsersLoader extends AbstractLoader
{
    public function init(): static
    {
        $this->transId = 'loader.users.message';
        $this->title = 'loader.users.phrase';
        $this->class = User::class;
        $this->prop = 'username';
        return $this;
    }

    public function prototype(object &$item, ?object $existsItem): void
    {
        if ($existsItem) {
            $item = $existsItem
                ->setSelectable($item->isSelectable())
                ->setVerified($item->isVerified())
                ->setEnabled($item->isEnabled());
        }
        $this->addGroups($item)->addPermissions($item);
        $country = $this->em->getRepository(Country::class)->findOneBy(['shortname' => $item->getCountry()->getShortname()]);
        $province = $this->em->getRepository(Province::class)->findOneBy(['name' => $item->getProvince()->getName()]);
        $password = $this->passwordHasher->hashPassword($item, $item->getPassword());
        $item
            ->setLastActivity(new \DateTime())
            ->setLastSeen(new \DateTime())
            ->setPassword($password)
            ->setProvince($province)
            ->setCountry($country);
    }

    protected function addGroups(User &$item): static
    {
        $shortnames = [];
        foreach ($item->getGroups() as $group) $shortnames[] = $group->getShortname();
        $groups = $this->em->getRepository(Group::class)->findByShortNames($shortnames);
        $item->getGroups()->clear();
        foreach ($groups as $group) $item->addGroup($group);
        return $this;
    }

    protected function addPermissions(User &$item): void
    {
        $shortnames = [];
        foreach ($item->getPermissions() as $role) $shortnames[] = $role->getShortname();
        $permissions = $this->em->getRepository(Role::class)->findByShortNames($shortnames);
        $item->getPermissions()->clear();
        foreach ($permissions as $role) $item->addGroup($role);
    }
}