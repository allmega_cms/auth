<?php

/**
 * This file is part of the Allmega Auth Bundle package.
 *
 * @copyright Allmega 
 * @package   Auth Bundle
 * @author    Eduard Jung <eddie@allmega.de>
 * @license   https://opensource.org/licenses/gpl-license.php GNU Public License
 */

namespace Allmega\AuthBundle\Utils\Loader\Loaders;

use Allmega\AuthBundle\Entity\{Group, GroupType, Role};
use Allmega\BlogBundle\Utils\Loader\Model\AbstractLoader;

class GroupsLoader extends AbstractLoader
{
    public function init(): static
    {
        $this->transId = 'loader.groups.message';
        $this->title = 'loader.groups.phrase';
        $this->class = Group::class;
        return $this;
    }

    public function prototype(object &$item, ?object $existsItem): void
    {
        $this->addGroupTypes($item)->addRoles($item);
        if ($existsItem) {
            $existsItem
                ->setSelectable($item->isSelectable())
                ->setSys($item->isSys());

            foreach ($item->getGrouptypes() as $groupType) {
                if (!$existsItem->getGrouptypes()->contains($groupType)) {
                    $existsItem->addGrouptype($groupType);
                }
            }
            
            foreach ($item->getRoles() as $role) {
                if (!$existsItem->getRoles()->contains($role)) {
                    $existsItem->addRole($role);
                }
            }
            $item = $existsItem;
        }
        $description = $this->trans("storage.group.{$item->getShortname()}.description", $this->currentPackage);
        $name = $this->trans("storage.group.{$item->getShortname()}.name", $this->currentPackage);
        $item->setName($name)->setDescription($description);
    }

    protected function addGroupTypes(Group &$item): static
    {
        if ($item->getGrouptypes()->count()) {
            $shortnames = [];
            foreach ($item->getGrouptypes() as $groupType) $shortnames[] = $groupType->getShortname();
            $groupTypes = $this->em->getRepository(GroupType::class)->findByShortnames($shortnames);
            $item->getGrouptypes()->clear();
            foreach ($groupTypes as $groupType) $item->addGrouptype($groupType);
        }
        return $this;
    }

    protected function addRoles(Group &$item): void
    {
        if ($item->getRoles()->count()) {
            $names = [];
            foreach ($item->getRoles() as $role) $names[] = $role->getName();
            $roles = $this->em->getRepository(Role::class)->findByNames($names);
            $item->getRoles()->clear();
            foreach ($roles as $role) $item->addRole($role);
        }
    }
}
