<?php

/**
 * This file is part of the Allmega Auth Bundle package.
 *
 * @copyright Allmega 
 * @package   Auth Bundle
 * @author    Eduard Jung <eddie@allmega.de>
 * @license   https://opensource.org/licenses/gpl-license.php GNU Public License
 */

namespace Allmega\AuthBundle\Utils\Loader\Loaders;

use Allmega\BlogBundle\Utils\Loader\Model\AbstractLoader;
use Allmega\AuthBundle\Entity\Country;

class CountriesLoader extends AbstractLoader
{
    public function init(): static
    {
        $this->transId = 'loader.countries.message';
        $this->title = 'loader.countries.phrase';
        $this->class = Country::class;
        return $this;
    }

    public function prototype(object &$item, ?object $existsItem): void
    {
        if ($existsItem) {
            $item = $existsItem->setSelectable($item->isSelectable())->setSys($item->isSys());
        }
        $path = $this->package . '.' . $item->getShortname();
        $description = $this->trans("storage.country.$path.description", $this->currentPackage);
        $name = $this->trans("storage.country.$path.name", $this->currentPackage);
        $item->setName($name)->setDescription($description);
    }
}