<?php

/**
 * This file is part of the Allmega Auth Bundle package.
 *
 * @copyright Allmega 
 * @package   Auth Bundle
 * @author    Eduard Jung <eddie@allmega.de>
 * @license   https://opensource.org/licenses/gpl-license.php GNU Public License
 */

namespace Allmega\AuthBundle\Utils\Loader\Loaders;

use Allmega\BlogBundle\Utils\Loader\Model\AbstractLoader;
use Allmega\AuthBundle\Entity\{Country,Province};

class ProvincesLoader extends AbstractLoader
{
    public function init(): static
    {
        $this->transId = 'loader.provinces.message';
        $this->title = 'loader.provinces.phrase';
        $this->class = Province::class;
        $this->prop = 'name';
        return $this;
    }

    public function prototype(object &$item, ?object $existsItem): void
    {
        if ($existsItem) {
            $item = $existsItem
                ->setSelectable($item->isSelectable())
                ->setSys($item->isSys());
        }
        $country = $this->em
            ->getRepository(Country::class)
            ->findOneBy(['shortname' => $item->getCountry()->getShortname()]);
        $item->setCountry($country);
    }
}