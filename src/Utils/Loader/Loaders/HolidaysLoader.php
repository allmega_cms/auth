<?php

/**
 * This file is part of the Allmega Auth Bundle package.
 *
 * @copyright Allmega 
 * @package   Auth Bundle
 * @author    Eduard Jung <eddie@allmega.de>
 * @license   https://opensource.org/licenses/gpl-license.php GNU Public License
 */

namespace Allmega\AuthBundle\Utils\Loader\Loaders;

use Allmega\BlogBundle\Utils\Loader\Model\AbstractLoader;
use Allmega\AuthBundle\Entity\{Holiday, Province};

class HolidaysLoader extends AbstractLoader
{
    public function init(): static
    {
        $this->transId = 'loader.holidays.message';
        $this->title = 'loader.holidays.phrase';
        $this->class = Holiday::class;
        return $this;
    }

    public function prototype(object &$item, ?object $existsItem): void
    {
        if (!$this->update) $this->addProvinces($item);
        if ($existsItem) {
            $item = $existsItem->setSelectable($item->isSelectable())->setSys($item->isSys());
        }
    }

    protected function addProvinces(Holiday &$item): void
    {
        if ($item->getProvinces()->count()) {
            $names = [];
            foreach ($item->getProvinces() as $province) $names[] = $province->getName();
            $provinces = $this->em->getRepository(Province::class)->findByNames($names);
            $item->getProvinces()->clear();
        } else {
            $provinces = $this->em->getRepository(Province::class)->findAll();
        }
        foreach ($provinces as $province) $item->addProvince($province);
    }
}
