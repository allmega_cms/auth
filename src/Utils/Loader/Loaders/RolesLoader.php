<?php

/**
 * This file is part of the Allmega Auth Bundle package.
 *
 * @copyright Allmega 
 * @package   Auth Bundle
 * @author    Eduard Jung <eddie@allmega.de>
 * @license   https://opensource.org/licenses/gpl-license.php GNU Public License
 */

namespace Allmega\AuthBundle\Utils\Loader\Loaders;

use Allmega\BlogBundle\Utils\Loader\Model\AbstractLoader;
use Allmega\AuthBundle\Entity\Role;

class RolesLoader extends AbstractLoader
{
    public function init(): static
    {
        $this->transId = 'loader.roles.message';
        $this->title = 'loader.roles.phrase';
        $this->class = Role::class;
        return $this;
    }

    public function prototype(object &$item, ?object $existsItem): void
    {
        if ($existsItem) {
            $item = $existsItem
                ->setName($item->getName())
                ->setSelectable($item->isSelectable())
                ->setSys($item->isSys());
        }
        $description = $this->trans("storage.role.{$item->getShortname()}", $this->currentPackage);
        $item->setDescription($description);
    }
}