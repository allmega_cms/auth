<?php

/**
 * This file is part of the Allmega Auth Bundle package.
 *
 * @copyright Allmega 
 * @package   Auth Bundle
 * @author    Eduard Jung <eddie@allmega.de>
 * @license   https://opensource.org/licenses/gpl-license.php GNU Public License
 */

namespace Allmega\AuthBundle\Utils\Loader;

use Allmega\AuthBundle\Utils\Loader\Loaders\{CountriesLoader, GroupsLoader, GroupTypesLoader, HolidaysLoader, ProvincesLoader, RolesLoader, UsersLoader};
use Allmega\BlogBundle\Utils\Loader\Model\AbstractStorage;

class Storage extends AbstractStorage
{
    protected array $props = ['countries', 'groups', 'grouptypes', 'holidays', 'provinces', 'roles', 'users'];
    protected array $grouptypes = [];
    protected array $countries = [];
    protected array $provinces = [];
    protected array $holidays = [];
    protected array $groups = [];
    protected array $roles = [];
    protected array $users = [];

    public function getLoaders(): array
    {
        $data = [
            RolesLoader::class => $this->roles,
            GroupTypesLoader::class => $this->grouptypes,
            CountriesLoader::class => $this->countries,
            ProvincesLoader::class => $this->provinces,
            HolidaysLoader::class => $this->holidays,
            GroupsLoader::class => $this->groups,
        ];

        if ($this->addUsers) $data[UsersLoader::class] = $this->users;
        return $data;
    }
}