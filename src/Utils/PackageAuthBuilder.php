<?php

/**
 * This file is part of the Allmega Auth Bundle package.
 *
 * @copyright Allmega 
 * @package   Auth Bundle
 * @author    Eduard Jung <eddie@allmega.de>
 * @license   https://opensource.org/licenses/gpl-license.php GNU Public License
 */

namespace Allmega\AuthBundle\Utils;

use Allmega\AuthBundle\Entity\{Group, GroupType, Role};

class PackageAuthBuilder
{
    private array $groupTypes = [];
    private array $groups = [];
    private array $roles = [];

    public function __construct(private readonly array $data)
    {
        $this->build();
    }

    private function build(): void
    {
        foreach ($this->data as $shortname => $groups) {
            if ($this->findItem($this->groupTypes, $shortname)) continue;

            $groupType = GroupType::build($shortname);
            $this->groupTypes[] = $groupType;

            foreach ($groups as $groupShortname => $rows) {
                if ($this->findItem($this->groups, $groupShortname)) continue;

                $group = Group::build($groupType, $groupShortname, $this->findRoles($rows));
                $this->groups[] = $group;
            }
        }
    }

    private function findItem(array $items, string $shortname): GroupType|Group|Role|null
    {
        foreach ($items as $item) {
            if ($item->getShortname() === $shortname) return $item;
        }
        return null;
    }

    private function findRoles(array $data): array
    {
        $roles = [];
        foreach ($data as $name => $shortname) {
            $role = $this->findItem($this->roles, $shortname);
            if (!$role) {
                $role = Role::build($name, shortname: $shortname);
                $this->roles[] = $role;
            }
            $roles[] = $role;
        }
        return $roles;
    }

    public function getGroupTypes(): array
    {
        return $this->groupTypes;
    }

    public function getGroups(): array
    {
        return $this->groups;
    }

    public function getRoles(): array
    {
        return $this->roles;
    }
}