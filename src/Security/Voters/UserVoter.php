<?php

/**
 * This file is part of the Allmega Auth Bundle package.
 *
 * @copyright Allmega 
 * @package   Auth Bundle
 * @author    Eduard Jung <eddie@allmega.de>
 * @license   https://opensource.org/licenses/gpl-license.php GNU Public License
 */

namespace Allmega\AuthBundle\Security\Voters;

use Allmega\AuthBundle\Entity\User;
use Allmega\BlogBundle\Model\{AllmegaVoterInterface, BaseVoterTrait};
use Symfony\Component\Security\Core\Authorization\Voter\Voter;

class UserVoter extends Voter implements AllmegaVoterInterface
{
    use BaseVoterTrait;
    
    protected string $changepassword = 'changepassword';
    protected string $verify  = 'verify';

    protected function supports($attribute, $subject): bool
    {
        $voterParams = $this->createVoterParams($attribute, $subject, 'auth-user', [$this->changepassword, $this->verify]);
        return $this->hasAttributeAndValidSubject($voterParams);
    }

    public function isGranted($attribute, $subject = null, ?User $user = null): bool
    {
        if (!$this->isSettedAndSupports($attribute, $subject)) return false;

        switch ($attribute) {
            case $this->changepassword:
            case $this->dashboard:
            case $this->delete:
            case $this->search:
            case $this->verify:
            case $this->list:
            case $this->show:
            case $this->edit:
            case $this->state:
            case $this->add:
                $result = $this->isAdmin($user);
                break;
            default:
                $result = false;
        }
        return $result;
    }

    public function isSubjectValid(mixed $subject): bool
    {
        return $subject instanceof User;
    }
}