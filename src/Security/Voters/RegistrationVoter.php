<?php

/**
 * This file is part of the Allmega Auth Bundle package.
 *
 * @copyright Allmega
 * @package   Auth Bundle
 * @author    Eduard Jung <eddie@allmega.de>
 * @license   https://opensource.org/licenses/gpl-license.php GNU Public License
 */

namespace Allmega\AuthBundle\Security\Voters;

use Allmega\AuthBundle\Entity\User;
use Allmega\BlogBundle\Model\{AllmegaVoterInterface, BaseVoterTrait};
use Symfony\Component\Security\Core\Authorization\Voter\Voter;

class RegistrationVoter extends Voter implements AllmegaVoterInterface
{
    use BaseVoterTrait;

    protected string $verify = 'verify';

    public function __construct(private readonly int $registrationEnabled) {}

    protected function supports($attribute, $subject): bool
    {
        $voterParams = $this->createVoterParams($attribute, $subject, 'auth-registration', [$this->verify]);
        return $this->hasAttributeAndValidSubject($voterParams);
    }

    public function isGranted($attribute, $subject = null, ?User $user = null): bool
    {
        if (!$this->isSettedAndSupports($attribute, $subject)) return false;

        switch ($attribute) {
            case $this->verify:
            case $this->add:
                $result = (bool) $this->registrationEnabled;
                break;
            default:
                $result = false;
        }
        return $result;
    }

    public function isSubjectValid(mixed $subject): bool
    {
        return false;
    }
}